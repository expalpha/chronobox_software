// koi2c.h -- i2c library K.Olchanski TRIUMF Dec 2023

#ifndef INCLUDE_KOI2C_H
#define INCLUDE_KOI2C_H

#include <stdio.h>
#include <stdint.h>

class KOI2C
{
public:
  KOI2C(); // ctor
  ~KOI2C(); // tor
public:
  bool i2c_open(const char* devname); // /dev/i2c-0 or /dev/i2c-1
  bool i2c_slave(int addr);
  int  i2c_read_byte(int chip_addr, int reg_addr);
  int  i2c_read_uint16_be(int chip_addr, int reg_addr);
  int  i2c_read_buf(int chip_addr, int reg_addr, char* buf, int nbytes);
  bool i2c_write_byte(int chip_addr, int reg_addr, int value);
  bool i2c_transfer(int chip_addr, int nwrite, const uint8_t write_data[], int nread, uint8_t read_data[]);
  bool i2c_write1(int chip_addr, uint8_t data);
  bool i2c_write2(int chip_addr, uint8_t data1, uint8_t data2);
  bool i2c_write3(int chip_addr, uint8_t data1, uint8_t data2, uint8_t data3);
public:
  int fFile = 0;
  int fSlave = 0;
  uint32_t fRetryCounter = 0;
  uint32_t fErrorCounter = 0;
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

