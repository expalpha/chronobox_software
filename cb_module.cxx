//
// MIDAS analyzer for chronobox data
//
// K.Olchanski
//

#include <stdio.h>
#include <vector>
#include <iostream>
#include <string>

#include "manalyzer.h"
#include "midasio.h"
#ifdef HAVE_ROOT
#include "TChronoChannelName.h"
#include "TTree.h"
#include "store_cb.h"
#endif


#include "TChronoChannel.h"

#include "unpack_cb.h"
#include "cb_flow.h"


class ChronoFlags
{
public:
   bool fPrint = false;
#ifdef HAVE_ROOT
   bool fDumpJsonChannelNames = false;
   bool fLoadJsonChannelNames = false;
   TString fLoadJsonFile="";
#endif
};

class CbChan
{
public:
   int fChan = 0;
   int fLastTe = 0;
   int fLastTs = 0;
   double fEpoch = 0;
   double fLastTime = 0;
   double fLastLeTime = 0;
   double fLastTeTime = 0;
   int fCountHits = 0;
   int fCountMissingEdge = 0;
   bool fExpectWraparound = false;
   int fCountMissingWraparound = 0;
   int fCountUnexpectedWraparound = 0;
};

class CbModule: public TARunObject
{
public:
   const double kTsFreq = 10.0e6; // 10 MHz

   ChronoFlags* fFlags;
   uint32_t fT0 = 0;
   uint32_t fTE = 0;
   int fCountWC = 0;
   int fCountMissingWC = 0;

   std::vector<CbChan*> fCbChan;

   CbUnpack fUnpackCb01;
   CbUnpack fUnpackCb02;
   CbUnpack fUnpackCb03;
   CbUnpack fUnpackCb04;
   CbUnpack fUnpackCbTRG;

   CbModule(TARunInfo* runinfo, ChronoFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="cb_module";
#endif
      printf("ctor, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fRunEventCounter = 0;

      fUnpackCb01.fCbNumInputs = 59;
      fUnpackCb02.fCbNumInputs = 59;
      fUnpackCb03.fCbNumInputs = 59;
      fUnpackCb04.fCbNumInputs = 59;
      fUnpackCbTRG.fCbNumInputs = 23;
   }

   ~CbModule()
   {

      printf("dtor!\n");
   }
  
   void BeginRun(TARunInfo* runinfo)
   {
      printf("BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      fRunEventCounter = 0;
#ifdef HAVE_ROOT
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      gDirectory->cd("/chrono");
      
      //Save chronobox channel names
      TChronoChannelName* name = new TChronoChannelName();
      TString ChannelName;
      TTree* ChronoBoxChannels = new TTree("ChronoBoxChannels","ChronoBoxChannels");
      ChronoBoxChannels->Branch("ChronoChannel",&name, 32000, 0);
      for (int i = 0; i < TChronoChannel::CBMAP.size(); i++)
      {
         for (const std::pair<const std::string, const int>& board: TChronoChannel::CBMAP)
         {
            //Write into tree in sorted order...
            if (i != TChronoChannel::CBMAP.at(board.first) )
               continue;

            delete name;
            name = NULL;
            if (fFlags->fLoadJsonChannelNames)
            {
               name = new TChronoChannelName(fFlags->fLoadJsonFile,board.first);
            }
            else
            {
               //Read chrono channel names from ODB (default behaviour)
               name = new TChronoChannelName(runinfo->fOdb,board.first);
            }
            //Dump name out to json
            if (fFlags->fDumpJsonChannelNames)
            {
               name->DumpToJson(runinfo->fRunNo);
            }
            if( fFlags->fPrint )
               name->Print();
            ChronoBoxChannels->Fill();

          }
       }
       delete name;
       ChronoBoxChannels->Write();
       delete ChronoBoxChannels;
#endif
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("EndRun, run %d\n", runinfo->fRunNo);
      printf("Counted %d events in run %d\n", fRunEventCounter, runinfo->fRunNo);
      double elapsed = fTE - fT0;
      double minutes = elapsed/60.0;
      double hours = minutes/60.0;
      printf("Elapsed time %d -> %d is %.0f sec or %f minutes or %f hours\n", fT0, fTE, elapsed, minutes, hours);
      if (fCountMissingWC) {
         printf("Wraparounds: %d, missing %d, cannot compute TS frequency\n", fCountWC, fCountMissingWC);
      } else {
         double tsbits = (1<<24);
         printf("Wraparounds: %d, approx rate %f Hz, ts freq %.1f Hz\n", fCountWC, fCountWC/elapsed, 0.5*fCountWC/elapsed*tsbits);
      }
      for (unsigned chan = 0; chan < fCbChan.size(); chan++) {
         CbChan* c = fCbChan[chan];
         if (c == NULL)
            continue;
         printf("Channel %3d: %d hits, %d missing edges, %d missing wrap, %d unexpected wrap\n", chan, c->fCountHits, c->fCountMissingEdge, c->fCountMissingWraparound, c->fCountUnexpectedWraparound);
      }
   }

   void NextSubrun(TARunInfo* runinfo)
   {
      printf("NextSubrun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
   }

   void PauseRun(TARunInfo* runinfo)
   {
      printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }


   std::vector<TCbFIFOEvent> BuildFIFOFlow(const std::string name, const CbHits& hits, int runNumber)
   {
      std::vector<TCbFIFOEvent> FIFOEvents;
      FIFOEvents.reserve(hits.size());
      for (const CbHit& hit: hits)
      {
         FIFOEvents.emplace_back(hit, name,runNumber);
      }
      return FIFOEvents;
   }

   int fPrevWC = 0;

   void AnalyzeCBS(TMEvent* event, TMBank* cbbank)
   {
      //      TMBank* cbbank = event->FindBank(bank_name.c_str());
      if (cbbank) {
         if (fT0 == 0) {
            fT0 = event->time_stamp;
         }
         fTE = event->time_stamp;
         int nwords = cbbank->data_size/4; // byte to uint32_t words
         uint32_t* cbdata = (uint32_t*)event->GetBankData(cbbank);
         for (int i=0; i<nwords; i++) {
            uint32_t w = cbdata[i];
            if ((w & 0xFF000000) == 0xFF000000) {
               int tsbits = (w>>16)&0xFF;
               int wc = w & 0xFFFF;

               if (fFlags->fPrint) {
                  printf("data %d: 0x%08x, timestamp wraparound counter %d, ts bits 0x%02x\n", i, w, wc, tsbits);
               }

               if (fPrevWC == 0) {
                  fPrevWC = wc;
               } else if (wc != fPrevWC + 1) {
                  printf("ERROR: timestamp wraparound counter jump %d -> %d\n", fPrevWC, wc);
                  fCountMissingWC ++;
                  fPrevWC = wc;
               } else {
                  fPrevWC = wc;
                  fCountWC ++;
               }
               if (tsbits == 0x80) {
                  for (unsigned chan = 0; chan < fCbChan.size(); chan++) {
                     CbChan* c = fCbChan[chan];
                     if (c == NULL)
                        continue;
                     c->fExpectWraparound = true;
                  }
               }
            } else if ((w & 0xFF000000) == 0xFE000000) {
               int scwc = w & 0xFFFF;

               if (fFlags->fPrint) {
                  printf("data %d: 0x%08x, scalers block %d words\n", i, w, scwc);
               }

               // this code is incomplete

            } else if ((w & 0x80000000) == 0x80000000) {
               int chan = (w>>24)&0x7F;
               int ts = w&0xFFFFFE;
               int te = w&1;

               while (chan >= (int)fCbChan.size()) {
                  fCbChan.push_back(NULL);
               }
               CbChan* c = fCbChan[chan];

               if (c == NULL) {
                  c = new CbChan;
                  c->fChan = chan;
                  c->fLastTe = te;
                  c->fLastTs = ts;
                  c->fEpoch = 0;
                  c->fCountHits = 1;

                  double time = ts/kTsFreq;
                  c->fLastTime = time;
                  if (te)
                     c->fLastTeTime = time;
                  else
                     c->fLastLeTime = time;

                  fCbChan[chan] = c;

                  if (fFlags->fPrint) {
                     printf("data %d: 0x%08x, chan %d, te %d, time 0x%06x, %f sec, first hit\n", i, w, chan, te, ts, time);
                  }
               } else {
                  if (te == c->fLastTe) {
                     printf("ERROR: missing edge: chan %d, edge %d -> %d\n", c->fChan, c->fLastTe, te);
                     c->fCountMissingEdge++;
                  }
                  if (c->fExpectWraparound) {
                     if (ts > c->fLastTs) {
                        printf("ERROR: timestamp did not wraparound: chan %d, ts %d -> %d\n", c->fChan, c->fLastTs, ts);
                        c->fCountMissingWraparound++;
                     } else {
                        c->fEpoch += (1<<24)/kTsFreq;
                     }
                     c->fExpectWraparound = false;
                  } else if (ts < c->fLastTs) {
                     printf("ERROR: timestamp wraparound: chan %d, ts %d -> %d\n", c->fChan, c->fLastTs, ts);
                     c->fCountUnexpectedWraparound++;
                     c->fEpoch += (1<<24)/kTsFreq;
                  }

                  double time = c->fEpoch + ts/kTsFreq;
                  double dt = time - c->fLastTime;
                  
                  if (fFlags->fPrint) {
                     printf("data %d: 0x%08x, chan %d, te %d, time 0x%06x, %f sec, plus %f usec, from last le %f, te %f\n", i, w, chan, te, ts, time, dt*1e6, (time-c->fLastLeTime)*1e6, (time-c->fLastTeTime)*1e6);
                  }

                  c->fCountHits ++;
                  c->fLastTe = te;
                  c->fLastTs = ts;
                  c->fLastTime = time;
                  if (te)
                     c->fLastTeTime = time;
                  else
                     c->fLastLeTime = time;
               }
            } else {
               printf("data %d: 0x%08x, do not know what this is\n", i, w);
            }
         }
      }
      // else
      //    std::cout<<"no bank: "<<bank_name<<std::endl;
   }
   void AnalyzeCbFifo(const char* name, CbUnpack* cb, TMEvent* event, TMBank* cbbank, TCbFlow* flow, int epoch_type)
   {
      int nwords = cbbank->data_size/4; // byte to uint32_t words
      uint32_t* cbdata = (uint32_t*)event->GetBankData(cbbank);

      CbHits hits;
      CbScalers scalers;

      cb->Unpack(cbdata, nwords, &hits, &scalers,epoch_type);

      if (hits.size() > 0) {
         if (fFlags->fPrint)
         {
            printf("Data from %s: ", name);
            PrintCbHits(hits);
         }
         flow->fCbHits[name] = std::move(hits);
      }

      if (scalers.size() > 0) {
         if (fFlags->fPrint)
         {
            printf("Data from %s: ", name);
            PrintCbScalers(scalers);
         }
         flow->fCbScalers[name] = std::move(scalers);
      }
   }

   TAFlowEvent* Analyze(TARunInfo* runinfo, TMEvent* event, TAFlags* flags, TAFlowEvent* flow)
   {

      fRunEventCounter++;

      //std::string bank_name = "CBS";

      TMBank* cbbank = 0;
      TCbFlow* cbflow = 0;
      event->FindAllBanks();

      cbbank = event->FindBank("CBF1");
      if (cbbank && runinfo->fRunNo >= 6546) //CB01 used old firmware until run 6546
      {
         if (!cbflow)
         {
            cbflow = new TCbFlow(flow);
            cbflow->fMidasTime = event->time_stamp;
            flow = cbflow;
         }
         AnalyzeCbFifo("cb01", &fUnpackCb01, event, cbbank, cbflow,4);
      }

      cbbank = event->FindBank("CBF2");
      if (cbbank)
      {
            if (!cbflow)
            {
               cbflow = new TCbFlow(flow);
               cbflow->fMidasTime = event->time_stamp;
               flow = cbflow;
            }
            AnalyzeCbFifo("cb02", &fUnpackCb02, event, cbbank, cbflow,4);
         }

         cbbank = event->FindBank("CBF3");
         if (cbbank)
         {
            if (!cbflow)
            {
               cbflow = new TCbFlow(flow);
               cbflow->fMidasTime = event->time_stamp;
               flow = cbflow;
            }
            AnalyzeCbFifo("cb03", &fUnpackCb03, event, cbbank, cbflow,4);
         }

         cbbank = event->FindBank("CBF4");
         if (cbbank)
         {
            if (!cbflow)
            {
               cbflow = new TCbFlow(flow);
               cbflow->fMidasTime = event->time_stamp;
               flow = cbflow;
            }
            AnalyzeCbFifo("cb04", &fUnpackCb04, event, cbbank, cbflow,4);
         }

         // TRG chronobox data
         cbbank = event->FindBank("CBFT");
         if (cbbank)
         {
            if (!cbflow)
            {
               cbflow = new TCbFlow(flow);
               cbflow->fMidasTime = event->time_stamp;
               flow = cbflow;
            }
            AnalyzeCbFifo("cbtrg", &fUnpackCbTRG, event, cbbank,cbflow,0);
         }

      return flow;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      const TCbFlow* cbflow = flow->Find<TCbFlow>();
      if (!cbflow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      TCbFIFOEventFlow* fifoflow = new TCbFIFOEventFlow(flow);
      fifoflow->fMidasTime = cbflow->fMidasTime;
      for (const std::pair<std::string,CbHits>& cbhits: cbflow->fCbHits)
      {
         fifoflow->fCbHits[cbhits.first] = BuildFIFOFlow(cbhits.first,cbhits.second,runinfo->fRunNo);
      }
      flow = fifoflow;

      return flow;
/*
      for (const std::pair<std::string,CbScalers>& cbscalers: cbflow->fCbScaler)
      {
         if (!fFIFOTree.count(cbscalers.first))
            CreateTree(cbscalers.first);
         FillTree(cbscalers.second);
      }
  */

   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      printf("AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }

   int fRunEventCounter;
};

class CbModuleFactory: public TAFactory
{
public:
   ChronoFlags fFlags;

   void Usage()
   {
      printf("\tCbModuleFactory Usage:\n");
      printf("\t--print-cb-data : print chronobox raw data\n");
#ifdef HAVE_ROOT
      printf("\t--dumpchronojson write out the chronobox channel list as json\n");
      printf("\t--loadchronojson filename.json override odb channel list with json\n");
#endif
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      printf("Arguments:\n");
      for (unsigned i=0; i<args.size(); i++) {
         printf("arg[%d]: [%s]\n", i, args[i].c_str());
         if (args[i] == "--print-cb-data") {
            fFlags.fPrint = true;
         }
#ifdef HAVE_ROOT
         if (args[i] == "--dumpchronojson")
            fFlags.fDumpJsonChannelNames = true;
         if (args[i] == "--loadchronojson")
         {
            fFlags.fLoadJsonChannelNames = true;
            i++;
            fFlags.fLoadJsonFile=args[i];
         }
#endif
      }
   }
   
   void Finish()
   {
      printf("Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new CbModule(runinfo, &fFlags);
   }
};

static TARegister tar(new CbModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
