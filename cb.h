// cb.h --- chronobox driver K.Olchanski TRIUMF Aug 2018

#ifndef INCLUDE_CB_H
#define INCLUDE_CB_H

#include <stdio.h>
#include <stdint.h>
#include <vector>

class Chronobox
{
 public:
   Chronobox(); // ctor
   ~Chronobox(); // dtor

 public:
   int cb_open();

 public: // fpga memory mapped bridge read/write
   uint32_t fpga_read32(int addr);
   void fpga_write32(int addr, uint32_t data);
   char* fpga_addr(int addr);

 public: // chronobox registers read/write, see https://daqstore.triumf.ca/AgWiki/index.php/Chronobox#Chronobox_firmware_registers
   uint32_t cb_read32(int ireg);
   void cb_write32(int ireg, uint32_t data);
   void cb_write32bis(int ireg, uint32_t data1, uint32_t data2);
   char* cb_addr(int ireg);

 public: // fpga h2f axi bus read/write
   uint64_t h2f_read64(int addr);
   void h2f_write64(int addr, uint64_t data);
   char* h2f_addr(int addr);

 public:
   void cb_reboot();

 public:
   void cb_reset_scalers();
   void cb_latch_scalers();

 public:
   void cb_int_clock();
   void cb_ext_clock();

 public:
   void cb_arm_sync();
   void cb_disarm_sync();

 public:
   int cb_read_input_num();
   void cb_read_scaler_begin();
   uint32_t cb_read_scaler(int iscaler);
   void cb_read_fifo(std::vector<uint32_t> *data, bool *overflow);
   void cb_set_lemo_out_mux(int ilemo, int ifunc);

 public:
   char* fBase = NULL;
   char* fBaseH2F = NULL;
   int fVersion = 0;
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

