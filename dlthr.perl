#!/usr/bin/perl -w

my $thh = "0x10";
my $thl = "0x00";

$thh = $ARGV[0] if $ARGV[0];
$thl = $ARGV[1] if $ARGV[1];

my $thv = (oct($thh)*256+oct($thl))/4096*80;

print "Setting the DL sipm board threasholds to $thh $thl dac, $thv mV!\n";

# single board through DL-DB, all DAC address jumpers should be open, i2c addr 0x0f

system "./dldb.perl --1";

system "i2ctransfer -y 1 w3\@0x0f 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0f 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x18 $thh $thl";

system "./dldb.perl --2";

system "i2ctransfer -y 1 w3\@0x0f 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0f 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x18 $thh $thl";

system "./dldb.perl --3";

system "i2ctransfer -y 1 w3\@0x0f 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0f 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x18 $thh $thl";

system "./dldb.perl --4";

system "i2ctransfer -y 1 w3\@0x0f 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0f 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x18 $thh $thl";

system "./dldb.perl --0";

exit 0;

# 1st board channels 1 and 2

system "i2ctransfer -y 1 w3\@0x0d 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0d 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0d 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0d 0x18 $thh $thl";

# 2nd board channels 3 and 4

system "i2ctransfer -y 1 w3\@0x0f 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0f 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0f 0x18 $thh $thl";

# 3nd board channels 5 and 6

system "i2ctransfer -y 1 w3\@0x0e 0x11 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0e 0x12 $thh $thl";
system "i2ctransfer -y 1 w3\@0x0e 0x14 0x00 0x00";
system "i2ctransfer -y 1 w3\@0x0e 0x18 $thh $thl";

# end
