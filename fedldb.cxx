/********************************************************************\

  Name:         fedldb.cxx
  Created by:   K.Olchanski

  Contents:     Frontend for the DL-DB dark light trigger counter power distribution board

\********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "tmfe.h"
#include "koi2c.h"

#include "midas.h"

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <math.h> // M_PI

#include "tmfe.h"

class EqDldb: public TMFeEquipment
{
public:
   KOI2C *fI2c = NULL;
   bool   fI2cFailed = false;

   EqDldb(const char *eqname, const char *eqfilename) // ctor
      : TMFeEquipment(eqname, eqfilename)
   {
      printf("EqDldb::ctor!\n");

      fEqConfBuffer = "";
      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 1;
      fEqConfTriggerMask = 0;
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = false;
      fEqConfReadOnlyWhenRunning = false;
      fEqConfEnablePoll = false; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
      fEqConfWriteCacheSize = 0;

      fI2c = new KOI2C();
   }

   ~EqDldb() // dtor
   {
      printf("EqDldb::dtor!\n");
   }

   void HandleUsage()
   {
      printf("EqDldb::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string> &args)
   {
      printf("EqDldb::HandleInit!\n");

      fOdbEqSettings->RSA("sipm_serial", NULL, true, 16, 32);
      //fOdbEqSettings->RIA("vbias_dac", NULL, true, 16);
      fOdbEqSettings->RIA("thra_dac",  NULL, true, 16);
      fOdbEqSettings->RIA("thrb_dac",  NULL, true, 16);
      //fOdbEqSettings->RU32("thr", NULL, true);
      fOdbEqSettings->RDA("vbias",  NULL, true, 16);

      bool ok = true;
      ok &= fI2c->i2c_open("/dev/i2c-1");

      if (!ok) {
         return TMFeErrorMessage("Cannot open i2c device!");
      }

      ok &= fI2c->i2c_write1(0x49, 0x84); // U19 temp adc
      ok &= fI2c->i2c_write1(0x4b, 0x84); // U20 temp adc

      ok &= fI2c->i2c_write3(0x48, 0x90, 0x50, 0); // U1 vbias dac, enable internal reference
      ok &= fI2c->i2c_write3(0x4A, 0x90, 0x50, 0); // U2 vbias dac, enable internal reference

      //system "i2ctransfer -y 1 w3\@0x48 15 0xD0 0x00";
      //system "i2ctransfer -y 1 w3\@0x4A 15 0xD0 0x00";

#if 0
      //ok &= fI2c->i2c_write3(0x48, 0x0F, 0x10, 0x00); // U1 set all DAC channels
      ok &= fI2c->i2c_write3(0x4A, 0x0F, 0x00, 0x00); // U2 set all DAC channels
      
      ok &= fI2c->i2c_write3(0x48, 0x00, 0xC4, 0x00); // port 1 green
      ok &= fI2c->i2c_write3(0x48, 0x02, 0xC4, 0x00); // port 2 red
      ok &= fI2c->i2c_write3(0x48, 0x04, 0xC4, 0x00); // port 3 blue
      ok &= fI2c->i2c_write3(0x48, 0x06, 0xC4, 0x00); // port 4 cyan
#endif

      if (ok) {
         EqSetStatus("Init Ok", "#00FF00");
      } else {
         I2cFailed("init failed");
      }

      return TMFeOk();
   }

   void I2cFailed(const char* message)
   {
      if (!fI2cFailed) {
         EqSetStatus(message, "#FF0000");
         fI2cFailed = true;
         fMfe->Msg(MERROR, "I2cFailed", "i2c failed: %s", message);
      }
   }

   double pow2(double x)
   {
      return x*x;
   }

   double pow3(double x)
   {
      return x*x*x;
   }

   double read_temp(int addr, int chan)
   {
      if (fI2cFailed)
         return 0;
      
      bool ok = true;
      uint8_t command = 0x84 + chan*16;
      ok &= fI2c->i2c_transfer(addr, 1, &command, 0, NULL);
      uint8_t data[2];
      ok &= fI2c->i2c_transfer(addr, 0, NULL, 2, data);
      if (!ok) {
         I2cFailed("read temp failed");
         return 0;
      }
      int raw = 256*data[0] + data[1];
      if (raw == 4095) {
         return 0;
      }

      double R25 = 5000.0; // R25 = 5kohm
      double RT = raw*R25/(4096-raw);
      double TCH = 28.54 * pow3(RT/R25) - 158.5 * pow2(RT/R25) + 474.8 * (RT/R25) - 319.85; // in degC
      return TCH;
   }

#if 0
   double read_dldb_vbias()
   {
      if (fI2cFailed)
         return 0;

      bool ok = true;

      const uint8_t vbias_write = 0x30;
      uint8_t data[3];
      
      ok &= fI2c->i2c_transfer(0x69, 1, &vbias_write, 0, NULL);
      while (1) {
         ok &= fI2c->i2c_transfer(0x69, 0, NULL, 3, data);
         if (data[2] == 0x30)
            break;
         // FIXME: this loop should have a limit!
      }

      if (!ok) {
         return 0;
      }
      
      int raw = 256*data[0] + data[1];
      uint8_t cnf = data[2];
      double v = raw/1000.0; // convert it to Volts from mV
      double R5=1000000.0;
      double R6=20000.0;
      double hv_raw = v/R6 * (R5+R6); // Recalculate HV at the input of R divider 1Mohm/20kohm
      double hv = hv_raw * 0.9177; // calibration
      //printf "DL-DB vbias: $data - $raw - $cnf - $v - $hv_raw - $hv\n";
      //printf("ok %d, vbias: 0x%02x %02x %02x\n", ok, vbias_data[0], vbias_data[1], vbias_data[2]);

      hv = hv*0.992554+0.170268;

      return hv;
   }
#endif

   double read_dldb_vbias18()
   {
      if (fI2cFailed)
         return 0;

      bool ok = true;

      ok &= fI2c->i2c_write1(0x69, 0x3C);

      if (!ok) {
         I2cFailed("read vbias18 failed");
         return 0;
      }

      int loop = 0;
      uint8_t data[4];

      for (; loop<50; loop++) {
         ok &= fI2c->i2c_transfer(0x69, 0, NULL, 4, data);
         if (!ok) {
            I2cFailed("read vbias18 failed");
            return 0; // error
         }
         if ((data[3] & 0x80) == 0)
            break; // new data is ready
         ::usleep(10000);
      }

      if (!ok) {
         I2cFailed("read vbias18 failed");
         return 0;
      }
      
      int raw = 256*256*data[0] + 256*data[1] + data[2];
      //uint8_t cnf = data[3];
      double v = raw*15.625*1e-6; // 15.625 μV
      double R5=1000000.0;
      double R6=20000.0;
      double hv_raw = v/R6 * (R5+R6); // Recalculate HV at the input of R divider 1Mohm/20kohm
      double hv = hv_raw * 0.9177; // calibration
      //printf "DL-DB vbias: $data - $raw - $cnf - $v - $hv_raw - $hv\n";

      //hv = hv*0.992554+0.170268;

      //printf("ok %d, vbias: 0x%02x %02x %02x %02x, loop %d, raw %d, cnf 0x%02x, v %.7f, vbias %.7f\n", ok, data[0], data[1], data[2], data[3], loop, raw, cnf, hv_raw, hv);

      return hv;
   }

#if 0
   double read_dldb_ibias()
   {
      if (fI2cFailed)
         return 0;

      bool ok = true;

      const uint8_t ibias_write = 0x10;
      uint8_t data[3];

      ok &= fI2c->i2c_transfer(0x69, 1, &ibias_write, 0, NULL);
      while (1) {
         ok &= fI2c->i2c_transfer(0x69, 0, NULL, 3, data);
         if (data[2] == 0x10)
            break;
         // FIXME: this loop should have a limit!
      }

      if (!ok) {
         return 0;
      }

      int raw = 256*data[0] + data[1];
      //uint8_t cnf = data[2];
      double v = raw/1000.0; // convert it to Volts from mV
      double i = v/60.0/0.5; // 60 is opamp gain, 0.5 is the shunt resistor
      //printf "DL-DB ibias: $data - $raw - $cnf - $v - $i\n";
      //printf("ok %d, ibias: 0x%02x %02x %02x\n", ok, ibias_data[0], ibias_data[1], ibias_data[2]);
      return i;
   }
#endif

   double read_dldb_ibias18()
   {
      if (fI2cFailed)
         return 0;

      bool ok = true;

      ok &= fI2c->i2c_write1(0x69, 0x1C);

      if (!ok) {
         I2cFailed("read ibias18 failed");
         return 0;
      }

      int loop = 0;
      uint8_t data[4];

      for (; loop<50; loop++) {
         ok &= fI2c->i2c_transfer(0x69, 0, NULL, 4, data);
         if (!ok) {
            I2cFailed("read ibias18 failed");
            return 0; // error
         }
         if ((data[3] & 0x80) == 0)
            break; // new data is ready
         ::usleep(10000);
      }

      if (!ok) {
         I2cFailed("read ibias18 failed");
         return 0;
      }
      
      int raw = 256*256*data[0] + 256*data[1] + data[2];
      //uint8_t cnf = data[3];
      double v = raw*15.625*1e-6; // 15.625 μV

      double i = v/60.0/0.5; // 60 is opamp gain, 0.5 is the shunt resistor

      //printf("ok %d, ibias: 0x%02x %02x %02x %02x, loop %d, raw %d, cnf 0x%02x, v %.7f, ibias %.7f\n", ok, data[0], data[1], data[2], data[3], loop, raw, cnf, v, i);

      return i;
   }

   bool i2c_disable()
   {
      if (fI2cFailed)
         return false;

      bool ok = true;
      uint8_t data[2];

      data[0] = 0x06; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);
      data[0] = 0x07; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);
      data[0] = 0x02; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);
      data[0] = 0x03; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);

      if (!ok) {
         I2cFailed("sipm board i2c port disable failed");
         return false;
      }

      return ok;
   }

   bool i2c_enable(int chan)
   {
      if (fI2cFailed)
         return false;

      bool ok = true;

      uint8_t data[2];

      data[0] = 0x06; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);
      data[0] = 0x07; data[1] = 0x00; ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);

      if (!ok) {
         I2cFailed("sipm board i2c port enable failed");
         return false;
      }

      data[0] = 0x02;
      data[1] = 0x00;
      if (chan < 8)
         data[1] = (1<<(chan-0));
      
      ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);

      data[0] = 0x03;
      data[1] = 0x00;
      if (chan >= 8)
         data[1] = (1<<(chan-8));
      
      ok &= fI2c->i2c_transfer(0x26, 2, data, 0, NULL);

      if (!ok) {
         I2cFailed("sipm board i2c port enable failed");
         return false;
      }

      return ok;
   }

   double read_sipm_vbias()
   {
      if (fI2cFailed)
         return 0;

      bool ok = true;

      uint8_t wr[] = { 0x1C };

      ok &= fI2c->i2c_transfer(0x6B, 1, wr, 0, NULL);

      if (!ok) {
         I2cFailed("sipm board read vbias failed");
         return 0;
      }

      for (int iloop=0; iloop<100; iloop++) {
         uint8_t data[6];
         ok &= fI2c->i2c_transfer(0x6B, 0, NULL, 6, data);
         int raw = 256*256*data[0] + 256*data[1] + data[2];
         double volt= raw*15.625*1e-6; // 15.625 μV
         int sta = data[3];
         int sta2 = data[4];
         bool good = (sta != sta2);
         double R33 = 1000000.0;
         double R34 =   20000.0;
         double hv = volt/(R34/(R33+R34));
         //print "$data raw $raw status $sta $sta2 good $good volt $volt hv $hv\n";
         if (!ok) {
            I2cFailed("sipm board read vbias failed");
            return 0;
         }
         if (good) {
            if (iloop > 0)
               printf("read_sipm_vbias: %d loops!\n", iloop);
            return hv;
         }
         ::usleep(10000);
      }

      I2cFailed("sipm board read vbias timeout waiting for new adc data");
      return 0;
   }

   bool write_sipm_thr(uint16_t thr)
   {
      if (fI2cFailed)
         return false;

      bool ok = true;

      uint8_t wdata[3];

      wdata[0] = 0x11;
      wdata[1] = 0x00;
      wdata[2] = 0x00;

      ok &= fI2c->i2c_transfer(0x0F, 3, wdata, 0, NULL);

      wdata[0] = 0x12;
      wdata[1] = 0xFF&(thr>>8);
      wdata[2] = 0xFF&(thr>>0);

      ok &= fI2c->i2c_transfer(0x0F, 3, wdata, 0, NULL);

      wdata[0] = 0x14;
      wdata[1] = 0x00;
      wdata[2] = 0x00;

      ok &= fI2c->i2c_transfer(0x0F, 3, wdata, 0, NULL);

      wdata[0] = 0x18;
      wdata[1] = 0xFF&(thr>>8);
      wdata[2] = 0xFF&(thr>>0);

      ok &= fI2c->i2c_transfer(0x0F, 3, wdata, 0, NULL);

      if (!ok) {
         I2cFailed("sipm board write threshold failed");
         return false;
      }

      return ok;
   }

   bool write_thr()
   {
      if (fI2cFailed)
         return false;

      bool ok = true;

      uint32_t thr = 0x1000;

      fOdbEqSettings->RU32("thr", &thr, true);

      double thr_mv = thr/4096.0*80.0;

      fOdbEqVariables->WD("thr_mv", thr_mv);

      fMfe->Msg(MINFO, "write_thr", "Set common threshold %.0f mV, DAC 0x%04x!", thr_mv, thr);

      ok &= i2c_enable(0);
      ok &= write_sipm_thr(thr);

      ok &= i2c_enable(1);
      ok &= write_sipm_thr(thr);

      ok &= i2c_enable(2);
      ok &= write_sipm_thr(thr);

      ok &= i2c_enable(3);
      ok &= write_sipm_thr(thr);

      return ok;
   }

   bool read_vbias(std::vector<double> &vbias)
   {
      if (fI2cFailed)
         return false;

      bool ok = true;
    
      // Calibration for old holder, non-bitelle boards (serials: 002, 003)
      ok &= i2c_enable(0);
      vbias[0] = read_sipm_vbias()*1.011390-0.020754;

      ok &= i2c_enable(1);
      vbias[1] = read_sipm_vbias()*1.012475-0.003688;

      // Calibration for new holder, mylar-wrapped, bitelle boards (serials: N/A)
      //ok &= i2c_enable(2);
      //vbias[2] = read_sipm_vbias()*1.012475-0.044187;

      //ok &= i2c_enable(3);
      //vbias[3] = read_sipm_vbias()*1.014049-0.069070;

      // Calibration for new holder, tedlar-wrapped, bitelle boards (serials: 9I, 10J)
      ok &= i2c_enable(2);
      vbias[2] = read_sipm_vbias()*1.012927-0.016082;

      ok &= i2c_enable(3);
      vbias[3] = read_sipm_vbias()*1.011334+0.006284;

      return ok;
   }

   bool read_temp(std::vector<double> &temp)
   {
      if (fI2cFailed)
         return false;

      temp[0] = read_temp(0x49, 0);
      temp[1] = read_temp(0x49, 4);  // Ben: temporary(?) fix to ch34 and ch56 temps being switched
      temp[2] = read_temp(0x49, 1);  
      //temp[1] = read_temp(0x49, 1);
      //temp[2] = read_temp(0x49, 4);
      temp[3] = read_temp(0x49, 5);
      //temp[3] = read_temp(0x4B, 0);

      return true;
   }

   void HandlePeriodic()
   {
      printf("EqDldb::HandlePeriodic!\n");

      if (fI2cFailed) {
         printf("i2c failed!\n");
         return;
      }

      double dldb_vbias = read_dldb_vbias18();
      double dldb_ibias = read_dldb_ibias18();

      std::vector<double> vbias(16);

      read_vbias(vbias);

      std::vector<double> temp(16);

      read_temp(temp);

      printf("temp %f %f %f %f, dldb vbias %.7f V, ibias %.7f A, sipm vbias %f %f %f %f\n",
             temp[0],
             temp[1],
             temp[2],
             temp[3],
             dldb_vbias,
             dldb_ibias,
             vbias[0],
             vbias[1],
             vbias[2],
             vbias[3]);

      fOdbEqVariables->WD("dldb_vbias", dldb_vbias);
      fOdbEqVariables->WD("dldb_ibias", dldb_ibias);
      fOdbEqVariables->WDA("vbias", vbias);
      fOdbEqVariables->WDA("temp", temp);
   }

   TMFeResult HandleBeginRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run %d!", run_number);
      return TMFeOk();
   }

   TMFeResult HandleEndRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run %d!", run_number);
      return TMFeOk();
   }

   bool HandlePoll()
   {
      return false;
   }

   void HandlePollRead()
   {
      // empty
   }

   TMFeResult HandleRpc(const char* cmd, const char* args, std::string& response)
   {
      fMfe->Msg(MINFO, "HandleRpc", "RPC cmd [%s], args [%s]", cmd, args);
      
      // RPC handler

      if (strcmp(cmd, "vbias_off") == 0) {
         printf("vbias_off!\n");
         bool ok = true;
         ok &= fI2c->i2c_write3(0x48, 0x0F, 0x00, 0x00); // U1 set all DAC channels
         ok &= fI2c->i2c_write3(0x4A, 0x0F, 0x00, 0x00); // U2 set all DAC channels

         if (!ok) response = "Failed";
         else response = "Success";
      } else if (strcmp(cmd, "vbias_on4") == 0) {
         printf("vbias_on4!\n");
         bool ok = true;
         //ok &= fI2c->i2c_write3(0x48, 0x0F, 0x00, 0x00); // U1 set all DAC channels

         int dac1 = 0;
         int dac2 = 0;
         int dac3 = 0;
         int dac4 = 0;

         //fOdbEqSettings->RIAI("vbias_dac", 0, &dac1);
         //fOdbEqSettings->RIAI("vbias_dac", 1, &dac2);
         //fOdbEqSettings->RIAI("vbias_dac", 2, &dac3);
         //fOdbEqSettings->RIAI("vbias_dac", 3, &dac4);

         double vbias1 = 0.0;
         double vbias2 = 0.0;
         double vbias3 = 0.0;
         double vbias4 = 0.0;

         fOdbEqSettings->RDAI("vbias", 0, &vbias1);
         fOdbEqSettings->RDAI("vbias", 1, &vbias2);
         fOdbEqSettings->RDAI("vbias", 2, &vbias3);
         fOdbEqSettings->RDAI("vbias", 3, &vbias4);

         if (vbias1 > 0) {
            dac1 = 1003.404*vbias1 - 2957.059;
            dac2 = 1011.490*vbias2 - 3354.255;
            dac3 = 1003.942*vbias3 - 2895.604;
            dac4 =  995.276*vbias4 - 2586.451;
         } else {
            dac1 = 0;
            dac2 = 0;
            dac3 = 0;
            dac4 = 0;
         }
         
         fMfe->Msg(MINFO, "HandleRpc", "Set vbias %.3f %.3f %.3f %.3f, DAC %d %d %d %d!", vbias1, vbias2, vbias3, vbias4, dac1, dac2, dac3, dac4);

         int max_dac = 53500; // roughly 56V max voltage

         if (dac1 > max_dac) dac1 = max_dac;
         if (dac2 > max_dac) dac2 = max_dac;
         if (dac3 > max_dac) dac3 = max_dac;
         if (dac4 > max_dac) dac4 = max_dac;

         std::vector<int> vbias_dac(16);

         vbias_dac[0] = dac1;
         vbias_dac[1] = dac2;
         vbias_dac[2] = dac3;
         vbias_dac[3] = dac4;

         fOdbEqVariables->WIA("vbias_dac", vbias_dac);

         ok &= fI2c->i2c_write3(0x48, 0x00, (dac1>>8)&0xFF, dac1&0xFF); // port 1 green
         ok &= fI2c->i2c_write3(0x48, 0x02, (dac2>>8)&0xFF, dac2&0xFF); // port 2 red
         ok &= fI2c->i2c_write3(0x48, 0x04, (dac3>>8)&0xFF, dac3&0xFF); // port 3 blue
         ok &= fI2c->i2c_write3(0x48, 0x06, (dac4>>8)&0xFF, dac4&0xFF); // port 4 cyan

         ok &= fI2c->i2c_write3(0x4A, 0x0F, 0x00, 0x00); // U2 set all DAC channels

         if (!ok) response = "Failed";
         else response = "Success";
      } else if (strcmp(cmd, "all_off") == 0) {
         printf("all_off!\n");
         bool ok = true;
         ok &= fI2c->i2c_write2(0x21, 0x06, 0);
         ok &= fI2c->i2c_write2(0x21, 0x07, 0);
         ok &= fI2c->i2c_write2(0x21, 0x02, 0x00);
         ok &= fI2c->i2c_write2(0x21, 0x03, 0x00);

         if (!ok) response = "Failed";
         else {
            EqSetStatus("All Off Ok", "#FFFFFF");
            fI2cFailed = false;
            response = "Success";
         }
      } else if (strcmp(cmd, "all_on") == 0) {
         printf("all_on!\n");
         bool ok = true;
         ok &= fI2c->i2c_write2(0x21, 0x06, 0);
         ok &= fI2c->i2c_write2(0x21, 0x07, 0);
         ok &= fI2c->i2c_write2(0x21, 0x02, 0xFF);
         ok &= fI2c->i2c_write2(0x21, 0x03, 0xFF);

         if (!ok) response = "Failed";
         else {
            EqSetStatus("All On Ok", "#00FF00");
            fI2cFailed = false;
            response = "Success";
         }
      } else if (strcmp(cmd, "on4") == 0) {
         printf("on4!\n");
         bool ok = true;
         ok &= fI2c->i2c_write2(0x21, 0x06, 0);
         ok &= fI2c->i2c_write2(0x21, 0x07, 0);
         ok &= fI2c->i2c_write2(0x21, 0x02, 0x0F);
         ok &= fI2c->i2c_write2(0x21, 0x03, 0x00);

         if (!ok) response = "Failed";
         else {
            EqSetStatus("On 4 Ok", "#00FF00");
            fI2cFailed = false;
            response = "Success";
         }
      } else if (strcmp(cmd, "on2") == 0) {
         printf("on2!\n");
         bool ok = true;
         ok &= fI2c->i2c_write2(0x21, 0x06, 0);
         ok &= fI2c->i2c_write2(0x21, 0x07, 0);
         ok &= fI2c->i2c_write2(0x21, 0x02, 0x03); // does not work!
         ok &= fI2c->i2c_write2(0x21, 0x03, 0x00);

         if (!ok) response = "Failed";
         else {
            fI2cFailed = false;
            response = "Success";
         }
      } else if (strcmp(cmd, "wthr") == 0) {
         EqSetStatus("Wthr!", "#00FF00");
         printf("wthr!\n");
         write_thr();
      } else {
         char tmp[256];
         time_t now = time(NULL);
         sprintf(tmp, "{ \"current_time\" : [ %d, \"%s\"] }", (int)now, ctime(&now));
         
         response = tmp;
      }

      return TMFeOk();
   }
};

// example frontend

class FeDldb : public TMFrontend
{
public:
   FeDldb() // ctor
   {
      printf("FeDldb::ctor!\n");
      FeSetName("fedldb");
   }

   void HandleUsage()
   {
      printf("FeDldb::HandleUsage!\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string> &args)
   {
      printf("FeDldb::HandleArguments!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string> &args)
   {
      printf("FeDldb::HandleFrontendInit!\n");
      FeAddEquipment(new EqDldb("dldb", __FILE__));
      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string> &args)
   {
      printf("FeDldb::HandleFrontendReady!\n");
      //fMfe->StartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeDldb::HandleFrontendExit!\n");
   };
};

// boilerplate main function

int main(int argc, char *argv[])
{
   FeDldb fedldb;
   return fedldb.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
