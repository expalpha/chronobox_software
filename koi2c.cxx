// koi2c.cxx --- i2c library K.Olchanski TRIUMF Dec 2023

#undef NDEBUG // this program requires working assert()

#include "koi2c.h"

#include <stdio.h>
#include <string.h> // memcpy()
#include <errno.h> // errno
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h>
#include <math.h> // fabs()
#include <sys/ioctl.h> // ioctl()
//#include <linux/i2c.h>
#include <linux/i2c-dev.h> // I2C_SLAVE
extern "C" {
#include <i2c/smbus.h> // apt install libi2c-dev
}

KOI2C::KOI2C() // ctor
{

}

KOI2C::~KOI2C() // dtor
{
   if (fFile > 0) {
      close(fFile);
   }
   
   fFile = 0;
   fSlave = 0;
}

bool KOI2C::i2c_open(const char* devname)
{
   assert(fFile == 0);
   
   fFile = open(devname, O_RDWR);
   
   if (fFile < 0) {
      fprintf(stderr, "Error: Could not open i2c device file \"%s\", errno %d (%s)\n", devname, errno, strerror(errno));
      return false;
   }

   int status = ioctl(fFile, I2C_RETRIES, 1);
   
   if (status != 0) {
      fprintf(stderr, "Error: Could not set I2C_RETRIES, errno %d (%s)\n", errno, strerror(errno));
      return false;
   }
   
   status = ioctl(fFile, I2C_TIMEOUT, 1);
   
   if (status != 0) {
      fprintf(stderr, "Error: Could not set I2C_TIMEOUT, errno %d (%s)\n", errno, strerror(errno));
      return false;
   }
   
   return true;
}

bool KOI2C::i2c_slave(int addr)
{
   if (addr == fSlave)
      return true;
   
   assert(fFile > 0);
   
   int status = ioctl(fFile, I2C_SLAVE, addr);
   
   if (status != 0) {
      fprintf(stderr, "Error: Could not set i2c slave address 0x%02x, errno %d (%s)\n", addr, errno, strerror(errno));
      return false;
   }
   
   fSlave = addr;
   
   return true;
}

int KOI2C::i2c_read_byte(int chip_addr, int reg_addr)
{
   bool ok = true;
   ok |= i2c_slave(chip_addr);
   int status = i2c_smbus_read_byte_data(fFile, reg_addr);
   if (status < 0) {
      fprintf(stderr, "Retry!\n");
      ::sleep(1);
      fRetryCounter++;
      status = i2c_smbus_read_byte_data(fFile, reg_addr);
      if (status < 0) {
         fprintf(stderr, "Bad after retry!\n");
         fErrorCounter++;
      }
   }
   ok |= (status == 0);
   if (!ok)
      return -1;
   return status;
}

int KOI2C::i2c_read_uint16_be(int chip_addr, int reg_addr)
{
   int hi = i2c_read_byte(chip_addr, reg_addr+0);
   int lo = i2c_read_byte(chip_addr, reg_addr+1);

   if (hi < 0)
      return -1;
   if (lo < 0)
      return -1;

   uint16_t v = ((hi&0xFF)<<8) | (lo&0xFF);

   return v;
}

int KOI2C::i2c_read_buf(int chip_addr, int reg_addr, char* buf, int nbytes)
{
   for (int i=0; i<nbytes; i++) {
      int v = i2c_read_byte(chip_addr, reg_addr+i);
      if (v < 0) {
         buf[i] = 0;
         return -1;
      }
      buf[i] = v;
   }

   return 0;
}

bool KOI2C::i2c_write_byte(int chip_addr, int reg_addr, int value)
{
   bool ok = true;
   ok |= i2c_slave(chip_addr);
   int status = i2c_smbus_write_byte_data(fFile, reg_addr, value);
   ok |= (status == 0);
   return ok;
}

bool KOI2C::i2c_transfer(int chip_addr, int nwrite, const uint8_t write_data[], int nread, uint8_t read_data[])
{
   struct i2c_msg msgs[2];

   int nmsgs = 0;

   if (nwrite > 0) {
      msgs[nmsgs].addr = chip_addr;
      msgs[nmsgs].flags = 0; // I2C_M_WR;
      msgs[nmsgs].len = nwrite;   
      msgs[nmsgs].buf = (__u8*)write_data;
      nmsgs++;
   }

   if (nread > 0) {
      msgs[nmsgs].addr = chip_addr;
      msgs[nmsgs].flags = I2C_M_RD;
      msgs[nmsgs].len = nread;   
      msgs[nmsgs].buf = (__u8*)read_data;
      for (int i=0; i<nread; i++)
         read_data[i] = 0xFF;
      nmsgs++;
   }

   struct i2c_rdwr_ioctl_data rdwr;
   rdwr.msgs = msgs;
   rdwr.nmsgs = nmsgs;

   int nmsgs_sent = ioctl(fFile, I2C_RDWR, &rdwr);

   if (nmsgs_sent < 0) {
      fprintf(stderr, "Error: Sending messages failed: errno %d (%s)\n", errno, strerror(errno));
      return false;
   } else if (nmsgs_sent < nmsgs) {
      fprintf(stderr, "Warning: only %d/%d messages were sent\n", nmsgs_sent, nmsgs);
      return false;
   }
   
   return true;
}

bool KOI2C::i2c_write1(int chip_addr, uint8_t data)
{
   uint8_t buf[1];
   buf[0] = data;
   return i2c_transfer(chip_addr, 1, buf, 0, NULL);
}

bool KOI2C::i2c_write2(int chip_addr, uint8_t data1, uint8_t data2)
{
   uint8_t buf[2];
   buf[0] = data1;
   buf[1] = data2;
   return i2c_transfer(chip_addr, 2, buf, 0, NULL);
}

bool KOI2C::i2c_write3(int chip_addr, uint8_t data1, uint8_t data2, uint8_t data3)
{
   uint8_t buf[3];
   buf[0] = data1;
   buf[1] = data2;
   buf[2] = data3;
   return i2c_transfer(chip_addr, 3, buf, 0, NULL);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

