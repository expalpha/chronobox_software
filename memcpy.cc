/*
g++ -O2 -m32 -static -Wall -Wuninitialized memcpy.cc -o memcpy32
g++ -O2 -m64 -static -Wall -Wuninitialized memcpy.cc -o memcpy64
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

int timediff(const struct timeval*v1,const struct timeval*v2)
{
  int diff_usec = v2->tv_usec - v1->tv_usec;
  int diff_sec  = v2->tv_sec - v1->tv_sec;
  return diff_usec + diff_sec*1000000;
}

double domemcpy(int size)
{
  int tsum0 = 0;
  int tsum1 = 0;
  void* src = malloc(size);
  void* dst = malloc(size);
  memset(src,0,size);
  memset(dst,0,size);
  while (1)
    {
      struct timeval t1, t2;
      gettimeofday(&t1,0);
      memcpy(dst,src,size);
      gettimeofday(&t2,0);
      int t = timediff(&t1,&t2);
      tsum0 += 1;
      tsum1 += t;

      if (tsum1 > 500000)
        break;
    }
  free(src);
  free(dst);
  return tsum1/(double)tsum0;
}

int main(int argc,char*argv[])
{
   for (int i=1024; ; i*=2)
    {
      double t = domemcpy(i);
      printf("memcpy %7d KiBytes: %6.0f MB/sec\n",i/1024,i/t);

      if (i>100*1024*1024)
        break;
    }
  return 0;
}

//end
