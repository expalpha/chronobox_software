//
// reboot the chronobox fpga
//
// K.Olchanski TRIUMF August 2018
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cb.h"

void usage()
{
   printf("Usage:\n");
   printf("test_cb.exe 0 # read chronobox register 0\n");
   printf("test_cb.exe 4 0xabcd # write chronobox register 4 value 0xabcd\n");
   printf("test_cb.exe fpga 0x10000 # read FPGA address 0x10000\n");
   printf("test_cb.exe fpga 0x10004 0x12345678 # write FPGA address 0x10004\n");
   printf("test_cb.exe h2f 0 # read FPGA H2F bridge address 0\n");
   printf("test_cb.exe h2f 0x10004 0x12345678 # write FPGA H2F bridge address 0x10004\n");
   printf("test_cb.exe clocks  # read and print clock status\n");
   printf("test_cb.exe extclk  # select external timestamp clock\n");
   printf("test_cb.exe intclk  # select internal timestamp clock\n");
   printf("test_cb.exe inputs  # read and print all inputs\n");
   printf("test_cb.exe lemo_in_a # set lemo bank A (LEMO 0..3) for input\n");
   printf("test_cb.exe lemo_in_b # set lemo bank B (LEMO 4..7) for input\n");
   printf("test_cb.exe lemo_out_a 0xF # output given value from lemo bank A (LEMO 0..3)\n");
   printf("test_cb.exe lemo_out_b 0xF # output given value from lemo bank B (LEMO 4..7)\n");
   printf("test_cb.exe lemo_out_clk N output timestamp clock on LEMO output N\n");
   printf("test_cb.exe arm_sync # arm timestamp sync, reset scalers, fifo, etc\n");
   printf("test_cb.exe disarm_sync # disarm timestamp sync\n");
   printf("test_cb.exe scalers # read and print all scalers\n");
   printf("test_cb.exe rates # read and print all the scales + rates\n");
   printf("test_cb.exe flows # read and print H2O flow rates\n");
   printf("test_cb.exe fifo  # read and print the FIFO data with scalers\n");
   printf("test_cb.exe tsfifo # read and print the FIFO data without scalers\n");
   printf("test_cb.exe reboot # reboot the fpga\n");
   printf("test_cb.exe test_h2f # test read and write of fpga h2f bridge\n");
   exit(1);
}

int main(int argc, char* argv[])
{
   setbuf(stdout, NULL);
   setbuf(stderr, NULL);
   Chronobox* cb = new Chronobox();
   int err = cb->cb_open();
   if (err) {
      printf("cb_open() failed, sorry.\n");
      exit(1);
   }

   for (int i=0; i<argc; i++) {
      printf("argv[%d]: %s\n", i, argv[i]);
   }

   if (argc <= 1) {
      usage(); // does not return
   }

   if (strcmp(argv[1], "reboot")==0) {
      cb->cb_reboot();
      exit(0);
   } else if (strcmp(argv[1], "fpga")==0) {
      if (argc == 3) {
         uint32_t addr = strtoul(argv[2], NULL, 0);
         uint32_t v = cb->fpga_read32(addr);
         printf("addr 0x%08x: 0x%08x (%d)\n", addr, v, v);
         exit(0);
      } else if (argc == 4) {
         uint32_t addr = strtoul(argv[2], NULL, 0);
         uint32_t v = strtoul(argv[3], NULL, 0);
         cb->fpga_write32(addr, v);
         printf("addr 0x%08x write 0x%08x\n", addr, v);
         exit(0);
      }
   } else if (strcmp(argv[1], "h2f")==0) {
      if (argc == 3) {
         uint32_t addr = strtoul(argv[2], NULL, 0);
         uint64_t v = cb->h2f_read64(addr);
         printf("h2f addr 0x%08x: 0x%016llx (%lld)\n", addr, v, v);
         exit(0);
      } else if (argc == 4) {
         uint32_t addr = strtoul(argv[2], NULL, 0);
         uint64_t v = strtoull(argv[3], NULL, 0);
         cb->h2f_write64(addr, v);
         printf("h2f addr 0x%08x write 0x%016llx\n", addr, v);
         exit(0);
      }
   } else if (strcmp(argv[1], "intclk")==0) {
      cb->cb_int_clock();
      exit(0);
   } else if (strcmp(argv[1], "extclk")==0) {
      cb->cb_ext_clock();
      exit(0);
   } else if (strcmp(argv[1], "clocks")==0) {
      while (1) {
         uint32_t reg18 = cb->cb_read32(0x18);
         uint32_t reg19 = cb->cb_read32(0x19);
         uint32_t reg1A = cb->cb_read32(0x1A);
         uint32_t reg1B = cb->cb_read32(0x1B);
         uint32_t reg1C = cb->cb_read32(0x1C);

         double ext_clk_ref = 10.0e-9 * reg18; // 100MHz reference, in sec
         double ext_freq = reg19/ext_clk_ref;

         double ts_clk_ref = 10.0e-9 * reg1A; // 100MHz reference, in sec
         double ts_freq = reg1B/ts_clk_ref;

         printf("clock status: ext_clk: counter 0x%08x, freq %.1f Hz, ts_clk: counter 0x%08x, freq %.1f Hz, PLL status 0x%08x\n", reg19, ext_freq, reg1B, ts_freq, reg1C);
         sleep(1);
      }
   } else if (strcmp(argv[1], "inputs")==0) {
      while (1) {
         uint32_t reg06 = cb->cb_read32(0x06);
         uint32_t reg09 = cb->cb_read32(0x09);
         uint32_t reg0A = cb->cb_read32(0x0A);

         uint16_t ecl_p1 = reg06 & 0xFFFF;
         uint16_t ecl_p2 = (reg06>>16)&0xFFFF;

         printf("inputs: ecl_p1: 0x%04x, ecl_p2: 0x%04x, lemo: 0x%02x (~0x%02x), gpio: 0x%05x\n", ecl_p1, ecl_p2, reg09, 0xFF&~reg09, reg0A);

         sleep(1);
      }
   } else if (strcmp(argv[1], "lemo_in_a")==0) {
      uint32_t reg0D = cb->cb_read32(0x0D);
      reg0D &= 0xF0FFFFFF; // disable LEMO_OUT tristates
      cb->cb_write32(0x0D, reg0D);
      printf("Lemo bank A tristates set for input\n");
   } else if (strcmp(argv[1], "lemo_in_b")==0) {
      uint32_t reg0D = cb->cb_read32(0x0D);
      reg0D &= 0x0FFFFFFF; // disable LEMO_OUT tristates
      cb->cb_write32(0x0D, reg0D);
      printf("Lemo bank B tristates set for input\n");
   } else if ((strcmp(argv[1], "lemo_out_a")==0) && (argc==3)) {
      uint32_t reg1D = cb->cb_read32(0x1D);
      reg1D &= 0xFFFF0000; // set output mux to function 0
      cb->cb_write32(0x1D, reg1D);
      uint32_t reg0D = cb->cb_read32(0x0D);
      reg0D |= 0x0F000000; // enable LEMO_OUT tristates
      cb->cb_write32(0x0D, reg0D);
      uint32_t reg0B = cb->cb_read32(0x0B);
      reg0B &= 0xF0;
      reg0B |= (0xF & strtoul(argv[2], NULL, 0)) << 0;
      cb->cb_write32(0x0B, reg0B);
      printf("Lemo bank A tristates set for output, lemo output 0x%02x\n", 0xFF&reg0B);
   } else if ((strcmp(argv[1], "lemo_out_b")==0) && (argc==3)) {
      uint32_t reg1D = cb->cb_read32(0x1D);
      reg1D &= 0x0000FFFF; // set output mux to function 0
      cb->cb_write32(0x1D, reg1D);
      uint32_t reg0D = cb->cb_read32(0x0D);
      reg0D |= 0xF0000000; // enable LEMO_OUT tristates
      cb->cb_write32(0x0D, reg0D);
      uint32_t reg0B = cb->cb_read32(0x0B);
      reg0B &= 0x0F;
      reg0B |= (0xF & strtoul(argv[2], NULL, 0)) << 4;
      cb->cb_write32(0x0B, reg0B);
      printf("Lemo bank B tristates set for output, lemo output 0x%02x\n", 0xFF&reg0B);
   } else if ((strcmp(argv[1], "arm_sync")==0)) {
      cb->cb_arm_sync();
      printf("cb_arm_sync: sync status 0x%08x\n", cb->cb_read32(32));
   } else if ((strcmp(argv[1], "disarm_sync")==0)) {
      cb->cb_disarm_sync();
      printf("cb_disarm_sync: sync status 0x%08x\n", cb->cb_read32(32));
   } else if ((strcmp(argv[1], "lemo_out_clk")==0) && (argc==3)) {
      int ilemo = strtoul(argv[2], NULL, 0);
      if (ilemo < 0 || ilemo > 7) {
         fprintf(stderr, "Error: lemo_out_clk LEMO output number %d out of range 0..7\n", ilemo);
      }
      uint32_t reg1D = cb->cb_read32(0x1D);
      reg1D &= ~((0xF)<<(ilemo*4));
      reg1D |=     (1)<<(ilemo*4); // set output mux to function 1
      cb->cb_write32(0x1D, reg1D);
      uint32_t reg0D = cb->cb_read32(0x0D);
      reg0D |= (1<<(24+ilemo)); // enable LEMO_OUT tristate
      cb->cb_write32(0x0D, reg0D);
      uint32_t reg0B = cb->cb_read32(0x0B);
      reg0B &= ~(1<<ilemo);
      cb->cb_write32(0x0B, reg0B);
      printf("Enable timestamp clock on LEMO output %d, reg1D: 0x%08x, reg0D: 0x%08x, reg0B: 0x%08x\n", ilemo, reg1D, reg0D, reg0B);
   } else if ((strcmp(argv[1], "fifo")==0) || (strcmp(argv[1], "tsfifo")==0)) {
      bool tsfifo = (strcmp(argv[1], "tsfifo")==0);
      uint32_t prev_ts = 0;
      int prev_ch = 0;
      int prev_te = 0;
      int num_scalers = 0;
      int count_scalers = 0;
      uint32_t last_v[60]={0};
      bool scalers_packet = false;

      std::vector<uint32_t> fifo_data;
      //std::vector<uint32_t> fifo_prev;
      
      bool overflow = false;
      bool first_time = true;

#if WPTR
      uint32_t w[4];
      int wptr = -1;
#endif

      while (1) {
         //if (fifo_data.size() > 0) {
         //   fifo_prev = fifo_data;
         //}
         fifo_data.clear();
         cb->cb_read_fifo(&fifo_data, &overflow);

         int nread = fifo_data.size();

         if (nread || overflow) {
            printf("fifo read %d words, fifo ever overflow: %d\n", nread, overflow);
         }

         if (first_time) {
            first_time = false;
            if (overflow) {
               overflow = false;
            }
         }
         
         if (nread == 0) {
            if (!tsfifo) {
               sleep(1);
               printf("latch scalers!\n");
               cb->cb_latch_scalers();
            } else {
               usleep(100000);
            }
            continue;
         }

         //bool corrupt = false;

         for (int i=0; i<nread; i++) {
            uint32_t v = fifo_data[i];
            printf("word %3d of %3d: 0x%08x", i, nread, v);
            if (scalers_packet) {
               printf(" scaler %d", count_scalers);
               if (v < last_v[count_scalers]) {
                  printf(" overflow OR corrupt, was 0x%x now 0x%08x (diff:%d)",last_v[count_scalers],v,v-last_v[count_scalers]);
                  //corrupt = true;
               }
               last_v[count_scalers] = v;
               count_scalers++;
               if (count_scalers == num_scalers) {
                  printf(" (last)");
                  scalers_packet = false;
               }
            } else if ((v & 0xFF000000) == 0xFF000000) {
               printf(" wrap around 0x%04x", v & 0xFFFF);
            } else if ((v & 0xFF000000) == 0xFE000000) {
               num_scalers = v & 0xFFFF;
               count_scalers = 0;
               printf(" packet of %d scalers", num_scalers);
               scalers_packet = true;
            } else {
               uint32_t ts = v & 0x00FFFFFE;
               int te = v & 1;
               int ch = (v & 0x7F000000)>>24;
#if 0
               if ((ts == prev_ts) && (te == prev_te)) {
                  if (ch != prev_ch) {
                     printf(" coinc");
                  } else {
                     printf(" dupe");
                  }
               } else if (ts < prev_ts) {
                  printf(" wrap");
               }
#endif
               if ((ch == prev_ch) && (te == prev_te)) {
                  printf(" missing edge");
               }

               printf(" ts: chan %d, ts %d, te %d", ch, ts, te);
               prev_ts = ts;
               prev_ch = ch;
               prev_te = te;

#if WPTR
               if (wptr < 0 && ch == 0 && te == 0)
                  wptr = 0;
               if (wptr >= 0) {
                  w[wptr++] = v;
                  if (wptr >= 4) {
                     wptr = 0;
                  }
               }
#endif
            }
            printf("\n");

#if WPTR
            if (wptr == 0) {
               uint32_t mask = 0;
               uint32_t ts0 = 0;

               for (int i=0; i<4; i++) {
                  uint32_t v = w[i];
                  uint32_t ts = v & 0x00FFFFFE;
                  int te = v & 1;
                  int ch = (v & 0x7F000000)>>24;

                  if (i==0) ts0 = ts;

                  if (ch == 0 && te == 0) mask |= 1;
                  if (ch == 0 && te == 1) mask |= 2;
                  if (ch == 1 && te == 0) mask |= 4;
                  if (ch == 1 && te == 1) mask |= 8;

                  if (ch == 0 && te == 0 && ts == ts0) mask |= 0x100;
                  if (ch == 0 && te == 1 && ts == ts0) mask |= 0x200;
                  if (ch == 1 && te == 0 && ts == ts0) mask |= 0x400;
                  if (ch == 1 && te == 1 && ts == ts0) mask |= 0x800;

                  if (ch == 0 && te == 0 && ts == ts0+2) mask |= 0x100;
                  if (ch == 0 && te == 1 && ts == ts0+2) mask |= 0x200;
                  if (ch == 1 && te == 0 && ts == ts0+2) mask |= 0x400;
                  if (ch == 1 && te == 1 && ts == ts0+2) mask |= 0x800;

                  if (ch == 0 && te == 0 && ts == ts0+4) mask |= 0x100;
                  if (ch == 0 && te == 1 && ts == ts0+4) mask |= 0x200;
                  if (ch == 1 && te == 0 && ts == ts0+4) mask |= 0x400;
                  if (ch == 1 && te == 1 && ts == ts0+4) mask |= 0x800;
               }

               printf("w: 0x%08x 0x%08x 0x%08x 0x%08x, mask 0x%08x\n", w[0], w[1], w[2], w[3], mask);

               if (mask != 0x0f0f) {
                  exit(1);
               }
            }
#endif
         }

#if 0
         if (corrupt) {
            for (unsigned i=0; i<fifo_prev.size(); i++) {
               uint32_t v = fifo_prev[i];
               printf("word %3d of %3d: 0x%08x (previous corrupt block)\n", i, fifo_prev.size(), v);
            }
            for (int i=0; i<nread; i++) {
               uint32_t v = fifo_data[i];
               printf("word %3d of %3d: 0x%08x (current corrupt block)\n", i, nread, v);
            }
         }
#endif
      }
      exit(0); 
   } else if (strcmp(argv[1], "scalers")==0) {
      int num = 1+cb->cb_read_input_num();
      while (1) {
         cb->cb_latch_scalers();
         int i = 7;
         printf("A cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         printf("B cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         cb->cb_read_scaler_begin();
         for (i=0; i<num; i++) {
            printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
         }
         sleep(1);
      }
      exit(0);
   } else if (strcmp(argv[1], "rates")==0) {
      int num = 1+cb->cb_read_input_num();
      int clk100 = num-1;
      uint32_t prevscalers[num];
      for (int i=0; i<num; i++) {
         prevscalers[i] = 0;
      }
      while (1) {
         // latch the counters
         cb->cb_latch_scalers();
         int i = 7;
         printf("A cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         printf("B cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         cb->cb_read_scaler_begin();
         uint32_t vclk = cb->cb_read_scaler(clk100);
         double delta_time = (double) vclk - (double)prevscalers[clk100];
         delta_time = delta_time / 1e8;
         for (i=0; i<num; i++) {
            uint32_t v = cb->cb_read_scaler(i);
            printf("dT:%f Idx:%3d Cnt: 0x%08x Diff:%d  Rate:%f\n"
                   , delta_time, i, v, v - prevscalers[i]
                   , (float) (v - prevscalers[i]) / delta_time);
            prevscalers[i] = v; 
         }
         prevscalers[clk100] = vclk; 
         sleep(1);
      }
   } else if (strcmp(argv[1], "flows")==0) {
      uint32_t prevscalers[58];
      while (1) {
         cb->cb_latch_scalers();
         int i = 7;
         printf("A cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         printf("B cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         cb->cb_read_scaler_begin();
         float delta_time = (float) cb->cb_read_scaler(58) - (float)prevscalers[58];
         delta_time = delta_time / 1e8;
         for (i=40; i<40+8; i++) {
            printf("dT:%f Idx:%3d Diff:%d  Rate:%f\n", delta_time, i, cb->cb_read_scaler(i) - prevscalers[i]
                   , (float) (cb->cb_read_scaler(i) - prevscalers[i]) / delta_time);
            prevscalers[i] = cb->cb_read_scaler(i); 
         }
         prevscalers[58] = cb->cb_read_scaler(58); 
         sleep(1);
      }
      exit(0);
   } else if (strcmp(argv[1], "test_h2f_1")==0) {
      while (1) {
         uint32_t addr = 0;
         uint64_t v1 = cb->h2f_read64(addr);
         uint64_t v2 = cb->h2f_read64(addr);
         uint64_t v3 = cb->h2f_read64(addr);
         uint64_t v4 = cb->h2f_read64(addr);
         uint64_t v5 = cb->h2f_read64(addr);
         uint64_t v6 = cb->h2f_read64(addr);
         uint64_t v7 = cb->h2f_read64(addr);
         uint64_t v8 = cb->h2f_read64(addr);
         printf("h2f addr 0x%08x: 0x%016llx 0x%016llx\n", addr, v1, v2);
         printf("0x%016llx 0x%016llx\n", v3, v4);
         printf("0x%016llx 0x%016llx\n", v5, v6);
         printf("0x%016llx 0x%016llx\n", v7, v8);
         ::sleep(1);
      }
   } else if (strcmp(argv[1], "test_h2f_2")==0) {
      uint64_t buf[1024];
      printf("buf addr 0x%p\n", &buf[0]);
      while (1) {
         uint32_t addr = 0;
         char* laddr = cb->h2f_addr(addr);
         memcpy(buf, laddr, 32*8);
         printf("h2f addr 0x%08x: 0x%016llx\n", addr, *(uint64_t*)buf);
         ::sleep(1);
      }
   } else if (argc == 2) {
      int ireg = strtoul(argv[1], NULL, 0);
      uint32_t v = cb->cb_read32(ireg);
      printf("cb register 0x%x: 0x%08x (%d)\n", ireg, v, v);
      exit(0);
   } else if (argc == 3) {
      int ireg = strtoul(argv[1], NULL, 0);
      uint32_t v = strtoul(argv[2], NULL, 0);
      cb->cb_write32(ireg, v);
      printf("cb register 0x%x write 0x%08x\n", ireg, v);
      exit(0);
   } else {
      usage();
      exit(0);
   }

#if 0
   //printf("SYSID_QSYS reg0: 0x%08x\n", cb->read32(SYSID_QSYS_BASE));
   //printf("SYSID_QSYS reg1: 0x%08x\n", cb->read32(SYSID_QSYS_BASE+4));

   if (0) {
      printf("read_data:  0x%08x\n", cb->fpga_read32(0x00+0));
      printf("write_data: 0x%08x\n", cb->fpga_read32(0x10+0));
      printf("addr:       0x%08x\n", cb->fpga_read32(0x20+0));
      
      cb->fpga_write32(0x10+0, 0x12345678);
      cb->fpga_write32(0x20+0, 0xbaadf00d);
      
      printf("read_data:  0x%08x\n", cb->fpga_read32(0x00+0));
      printf("write_data: 0x%08x\n", cb->fpga_read32(0x10+0));
      printf("addr:       0x%08x\n", cb->fpga_read32(0x20+0));
      
      cb->fpga_write32(0x10+0, 0xFFFFFFFF);
      cb->fpga_write32(0x20+0, 0x80000001);
   }
   
   printf("read_data:  0x%08x\n", cb->fpga_read32(0x00+0));
   printf("write_data: 0x%08x\n", cb->fpga_read32(0x10+0));
   printf("addr:       0x%08x\n", cb->fpga_read32(0x20+0));
   
   cb->cb_write32(1, 0xFFFFFFFF);
   sleep(1);
   cb->cb_write32(1, 0);
   cb->cb_write32(4, 0xfeedf00d);
   
   int i;
   for (i=0; i<10; i++) {
      printf("cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
   }
   
   if (1) {
      uint32_t fwrev = cb->cb_read32(0);
      printf("cb fw rev: 0x%08x\n", fwrev);
      printf("fpga reconfigure!\n");
      sleep(1);
      cb->cb_write32(0xE, ~fwrev);
      //printf("cb regE: 0x%08x\n", cb->cb_read32(0xE));
      sleep(1);
      printf("cb fw rev: 0x%08x\n", cb->cb_read32(0));
      sleep(1);
      printf("cb fw rev: 0x%08x\n", cb->cb_read32(0));
      sleep(1);
      printf("cb fw rev: 0x%08x\n", cb->cb_read32(0));
      printf("done.\n");
      exit(1);
   }
   
   cb->cb_reset_scalers();
   
   int j;
   for (j=0; j<5; j++) {
      cb->cb_latch_scalers();
      i = 7;
      printf("A cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
      printf("B cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
      cb->cb_read_scaler_begin();
      i = 0;
      printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
      i = 1;
      printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
      i = 15;
      printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
      i = 16;
      printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
      i = 17;
      printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
      sleep(1);
   }
   
   if (1) {
      while (1) {
         cb->cb_latch_scalers();
         int i = 7;
         printf("A cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         printf("B cb reg %2d: 0x%08x\n", i, cb->cb_read32(i));
         cb->cb_read_scaler_begin();
         for (i=0; i<32+8+18+1; i++) {
            printf("C cb sc %3d: 0x%08x\n", i, cb->cb_read_scaler(i));
         }
         sleep(1);
      }
   }
   
   if (1) {
      for (i=0; i<8; i++) {
         printf("turning on LED%d\n", i+1);
         cb->cb_write32(1, 1<<(i+8));
         sleep(1);
      }
      
      cb->cb_write32(1, 0);
   }
   
   if (1) {
      printf("enable LEMO outputs:\n");
      
      cb->cb_write32(0xB, 0xffffffff); // lemo out
      cb->cb_write32(0xC, 0xffffffff); // gpio out
      cb->cb_write32(0xD, 0xffffffff); // out enable
      
      while (1) {
         printf("on!\n");
         cb->cb_write32(0xB, 0xffffffff); // lemo out
         cb->cb_write32(0xC, 0xffffffff); // gpio out
         sleep(2);
         printf("off!\n");
         cb->cb_write32(0xB, 0x00000000); // lemo out
         cb->cb_write32(0xC, 0x00000000); // gpio out
         sleep(2);
      }
   }
   
   if (1) {
      printf("print ECL, LEMO and GPIO inputs:\n");
      
      cb->cb_write32(0xD, 0x000000000); // out enable
      
      while (1) {
         printf("ECL: 0x%08x, LEMO 0x%04x, GPIO: 0x%08x\n",
                cb->cb_read32(0x6),
                cb->cb_read32(0x9),
                cb->cb_read32(0xA)
                );
         sleep(1);
      }
   }
#endif
   
   delete cb;
   return 0;
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
