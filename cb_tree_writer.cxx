//
// MIDAS analyzer for chronobox data, compliment to cb_module that writes to root on a set period
//
// JTK McKENNA
//

#include <stdio.h>
#include <vector>
#include <list>
#include <iostream>
#include <string>

#include "manalyzer.h"
#include "midasio.h"
#ifdef HAVE_ROOT
#include "TChronoChannelName.h"
#include "TTree.h"
#include "store_cb.h"
#endif


#include "TChronoChannel.h"

#include "cb_flow.h"


class ChronoBox_WriterFlags
{
public:
   bool fDumpJsonChannelNames = false;
   bool fLoadJsonChannelNames = false;
   bool fPrint = false;
   TString fLoadJsonFile="";
};


#define ORIGINAL 1
#define CHANNEL_PER_TREE 2
#define DOUBLE_PER_TREE 3
#define TREEMODE CHANNEL_PER_TREE

#if TREEMODE == DOUBLE_PER_TREE
class TreeBranchDouble
{
   public:
   TTree* fTree;
   TBranch* fBranch;
   Double_t* fData;
   TreeBranchDouble(TTree* tree, TBranch* branch, Double_t* data):
      fTree(tree), fBranch(branch), fData(data)
      {
      }
   ~TreeBranchDouble()
   {
      delete fTree;
      delete fBranch;
      delete fData;
   }
};
#endif

class ChronoBox_Writer: public TARunObject
{
public:
   ChronoBox_WriterFlags* fFlags;
   std::map<std::string,std::list<std::vector<std::shared_ptr<TCbFIFOEvent>>>> fCbFIFOEventBuffer;
   uint32_t fLastFlush;

#ifdef HAVE_ROOT
#if TREEMODE == ORIGINAL
   std::map<std::string,std::pair<TTree*,TBranch*>> fFIFOTrees;
#endif
#if TREEMODE == CHANNEL_PER_TREE
   std::map<std::string,std::vector<std::pair<TTree*,TBranch*>>> fFIFOTrees;
#endif
#if TREEMODE == DOUBLE_PER_TREE
   std::map<std::string,std::vector<TreeBranchDouble*>> fFIFOTrees;
#endif
   //std::map<std::string,std::pair<TTree*,TCbScalerEvent*>> fScalerTrees;
#endif
   ChronoBox_Writer(TARunInfo* runinfo, ChronoBox_WriterFlags* flags)
      : TARunObject(runinfo), fFlags(flags)
   {
      fLastFlush = 0;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="ChronoBox_Writer";
#endif
      printf("ctor, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
   }

   ~ChronoBox_Writer()
   {

      printf("dtor!\n");
   }
  
   void BeginRun(TARunInfo* runinfo)
   {
      printf("BeginRun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd();
      gDirectory->cd("/chrono");
      
      //Save chronobox channel names
      TChronoChannelName* name = new TChronoChannelName();
      TString ChannelName;
      TTree* ChronoBoxChannels = new TTree("ChronoBoxChannels","ChronoBoxChannels");
      ChronoBoxChannels->Branch("ChronoChannel",&name, 32000, 0);
      for (size_t i = 0; i < TChronoChannel::CBMAP.size(); i++)
      {
         for (const auto& board: TChronoChannel::CBMAP)
         {
            //Write into tree in sorted order...
            if ((int)i != TChronoChannel::CBMAP.at(board.first) )
               continue;

            delete name;
            name = NULL;
            if (fFlags->fLoadJsonChannelNames)
            {
               name = new TChronoChannelName(fFlags->fLoadJsonFile,board.first);
            }
            else
            {
               //Read chrono channel names from ODB (default behaviour)
               name = new TChronoChannelName(runinfo->fOdb,board.first);
            }
            //Dump name out to json
            if (fFlags->fDumpJsonChannelNames)
            {
               name->DumpToJson(runinfo->fRunNo);
            }
            if( fFlags->fPrint )
               name->Print();
            ChronoBoxChannels->Fill();

          }
       }
       delete name;
       ChronoBoxChannels->Write();
       delete ChronoBoxChannels;
   }

   void EndRun(TARunInfo* runinfo)
   {
      printf("EndRun, run %d\n", runinfo->fRunNo);
 
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd();
#if TREEMODE == ORIGINAL
      for (std::pair<std::string,std::pair<TTree*,TBranch*>> trees: fFIFOTrees)
      {
         trees.second.first->Write();
         delete trees.second.first;
         //delete trees.second.second;
      }
#endif
#if TREEMODE == CHANNEL_PER_TREE
      for (std::pair<std::string,std::vector<std::pair<TTree*,TBranch*>>> treeList: fFIFOTrees)
      {
         for (std::pair<TTree*,TBranch*>& trees: treeList.second)
         {
            trees.first->Write();
            delete trees.first;
         }
      }
#endif
#if TREEMODE == DOUBLE_PER_TREE

      for (std::pair<std::string,std::vector<TreeBranchDouble*>> treeList: fFIFOTrees)
      {
         for (TreeBranchDouble* trees: treeList.second)
         {
            trees->fTree->Write();
            delete trees;
         }
      }
#endif

      fFIFOTrees.clear();
   }

   void NextSubrun(TARunInfo* runinfo)
   {
      printf("NextSubrun, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
   }

   void PauseRun(TARunInfo* runinfo)
   {
      printf("PauseRun, run %d\n", runinfo->fRunNo);
   }

   void ResumeRun(TARunInfo* runinfo)
   {
      printf("ResumeRun, run %d\n", runinfo->fRunNo);
   }

   void CreateFIFOTree(TARunInfo* runinfo, const std::string name)
   {
      std::string tree_name = "ChronoBox_" + name;
      std::cout <<"Create: " << tree_name << std::endl;
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
      runinfo->fRoot->fOutputFile->cd();
#if TREEMODE == ORIGINAL
      TTree* t = new TTree(tree_name.c_str(), tree_name.c_str());
      TCbFIFOEvent* c = new TCbFIFOEvent();
      TBranch* b = t->Branch("FIFOData",c, 32000, 0);
      delete c;
      fFIFOTrees[name] = std::make_pair(t,b);
#endif
#if TREEMODE == CHANNEL_PER_TREE
      runinfo->fRoot->fOutputFile->mkdir(name.c_str());
      runinfo->fRoot->fOutputFile->cd(name.c_str());
      for (size_t i = 0; i < 64; i++)
      {
         std::string split_tree_name(tree_name);
         split_tree_name += std::string("_") + std::to_string(i);
         TTree* t = new TTree(split_tree_name.c_str(),split_tree_name.c_str());
         TCbFIFOEvent* c = new TCbFIFOEvent();
         TBranch* b = t->Branch("FIFOData",c, 32000, 0);
         delete c;
         fFIFOTrees[name].emplace_back(t,b);
      }
#endif
#if TREEMODE == DOUBLE_PER_TREE
      runinfo->fRoot->fOutputFile->mkdir(name.c_str());
      runinfo->fRoot->fOutputFile->cd(name.c_str());
      for (size_t i = 0; i < 64; i++)
      {
         std::string split_tree_name(tree_name);
         split_tree_name += std::to_string(i);
         TTree* t = new TTree(split_tree_name.c_str(),split_tree_name.c_str());
         Double_t* c = new Double_t();
         TBranch* b = t->Branch("FIFOData",c, 32000, 0);
         //TreeBranchDouble constructor:
         fFIFOTrees[name].emplace_back(new TreeBranchDouble(t,b,c));
      }
#endif
      return;
   }

   void QueueFifoData(const std::string name, std::vector<std::shared_ptr<TCbFIFOEvent>> fifo)
   {
      fCbFIFOEventBuffer[name].emplace_back(std::move(fifo));      
   }
   void FlushBuffer(TARunInfo* runinfo)
   {
      for (std::pair<std::string, std::list<std::vector<std::shared_ptr<TCbFIFOEvent>>>> fifo: fCbFIFOEventBuffer)
      {
         const std::string& name = fifo.first;

         if (!fFIFOTrees.count(name))
            CreateFIFOTree(runinfo,name);
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#if TREEMODE == ORIGINAL
         std::pair<TTree*,TBranch*>& tree = fFIFOTrees[name];
         TCbFIFOEvent* e;
         tree.first->SetBranchAddress("FIFOData",&e);
#endif
#if TREEMODE == CHANNEL_PER_TREE
         std::vector<std::pair<TTree*,TBranch*>>& trees = fFIFOTrees[name];
         TCbFIFOEvent* e;
#endif
#if TREEMODE == DOUBLE_PER_TREE
         std::vector<TreeBranchDouble*>& trees = fFIFOTrees[name];
#endif
         for (std::vector<std::shared_ptr<TCbFIFOEvent>>& FIFOEvents: fifo.second)
         {
            for (std::shared_ptr<TCbFIFOEvent> event: FIFOEvents)
            {
#if TREEMODE == ORIGINAL
               e = event.get();
               tree.first->Fill();
#endif
#if TREEMODE == CHANNEL_PER_TREE
               size_t position = event->GetChannel();
               std::pair<TTree*,TBranch*> tree = trees.at(position);
               e = event.get();
               tree.first->SetBranchAddress("FIFOData",&e);
               tree.first->Fill();
#endif
#if TREEMODE == DOUBLE_PER_TREE
               size_t position = event->GetChannel();
               double time = event.GetRunTime();
               TreeBranchDouble* tree = trees.at(position);
               *tree->fData = time;
               tree->fTree->Fill();
#endif
            }

            //FIFOEvents.clear();
         }
         //fifo.second.clear();
      }
      fCbFIFOEventBuffer.clear();
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
#ifdef HAVE_ROOT
      TCbFIFOEventFlow* fifoflow = flow->Find<TCbFIFOEventFlow>();
      if (!fifoflow)
      {
#ifdef HAVE_MANALYZER_PROFILER
         *flags|=TAFlag_SKIP_PROFILE;
#endif
         return flow;
      }
      uint32_t time_now = 0;
      for (std::pair<std::string,std::vector<std::shared_ptr<TCbFIFOEvent>>> cbhits: fifoflow->fCbHits)
      {
         //WARNING THIS IS DESTRUCTIVE TO THE FLOW! (ie std::move)
         if (cbhits.second.size())
             time_now = cbhits.second.front()->GetMidasTime();
         QueueFifoData(cbhits.first,cbhits.second);
      }

      auto dt = time_now - fLastFlush;
      if ( dt < 10) // up to ~1 write flush per 10 seconds
      {
         //std::cout << "CB: skip!"<<std::endl;
         return flow;
      }
      fLastFlush = time_now;
      FlushBuffer(runinfo);
#endif
      return flow;
   }

   void AnalyzeSpecialEvent(TARunInfo* runinfo, TMEvent* event)
   {
      printf("AnalyzeSpecialEvent, run %d, event serno %d, id 0x%04x, data size %d\n", runinfo->fRunNo, event->serial_number, (int)event->event_id, event->data_size);
   }

   int fRunEventCounter;
};

class ChronoBox_WriterFactory: public TAFactory
{
public:
   ChronoBox_WriterFlags fFlags;

   void Usage()
   {
      printf("\tChronoBox_WriterFactory Usage:\n");
      printf("\t--print-cb-data : print chronobox raw data\n");
#ifdef HAVE_ROOT
      printf("\t--dumpchronojson write out the chronobox channel list as json\n");
      printf("\t--loadchronojson filename.json override odb channel list with json\n");
#endif
   }
   void Init(const std::vector<std::string> &args)
   {
      printf("Init!\n");
      printf("Arguments:\n");
      for (unsigned i=0; i<args.size(); i++) {
         printf("arg[%d]: [%s]\n", i, args[i].c_str());
#ifdef HAVE_ROOT
         if (args[i] == "--dumpchronojson")
            fFlags.fDumpJsonChannelNames = true;
         if (args[i] == "--loadchronojson")
         {
            fFlags.fLoadJsonChannelNames = true;
            i++;
            fFlags.fLoadJsonFile=args[i];
         }
#endif
      }
   }
   
   void Finish()
   {
      printf("Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new ChronoBox_Writer(runinfo, &fFlags);
   }
};

static TARegister tar(new ChronoBox_WriterFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
