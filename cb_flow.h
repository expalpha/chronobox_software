#include "unpack_cb.h"

#include "manalyzer.h"

#include <map>

class TCbFlow: public TAFlowEvent
{
   public:
      //CBHits per Chronobox label (cb01,cb02,cb03,cb04,trg...)
      std::map<std::string,CbHits> fCbHits;
      std::map<std::string,CbScalers> fCbScalers;
   public:
   TCbFlow(TAFlowEvent* flow) //ctor
     : TAFlowEvent(flow)
   {
   }

   ~TCbFlow() //dtor
   {
      fCbHits.clear();
      fCbScalers.clear();
   }
};

#ifdef HAVE_ROOT

#include "store_cb.h"


class TCbFIFOEventFlow: public TAFlowEvent
{
   public:
      //CBHits per Chronobox label (cb01,cb02,cb03,cb04,trg...)
      std::map<std::string,std::vector<std::shared_ptr<TCbFIFOEvent>>> fCbHits;
   public:
   TCbFIFOEventFlow(TAFlowEvent* flow) //ctor
     : TAFlowEvent(flow)
   {
   }

   ~TCbFIFOEventFlow() //dtor
   {
      fCbHits.clear();
   }
};

#endif
