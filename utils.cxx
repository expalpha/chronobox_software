#include <assert.h>
#include "midas.h"

extern char* frontend_name;
extern INT frontend_index;
extern HNDLE hDB;
extern EQUIPMENT equipment[];

int odbReadAny(const char*name,int index,int tid,void* value,int valueLength = 0)
{
  int status;
  int size = rpc_tid_size(tid);
  HNDLE hdir = 0;
  HNDLE hkey;

  if (valueLength > 0)
    size = valueLength;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      status = db_get_data_index(hDB, hkey, value, &size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot read \'%s\'[%d] of type %d from odb, db_get_data_index() status %d", name, index, tid, status);
          return -1;
        }

      return 0;
    }
  else if (status == DB_NO_KEY)
    {
      cm_msg(MINFO, frontend_name, "Creating \'%s\'[%d] of type %d", name, index, tid);

      status = db_create_key(hDB, hdir, (char*)name, tid);
      if (status != SUCCESS)
        {
          cm_msg (MERROR, frontend_name, "Cannot create \'%s\' of type %d, db_create_key() status %d", name, tid, status);
          return -1;
        }

      status = db_find_key (hDB, hdir, (char*)name, &hkey);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", name, status);
          return -1;
        }

      status = db_set_data_index(hDB, hkey, value, size, index, tid);
      if (status != SUCCESS)
        {
          cm_msg(MERROR, frontend_name, "Cannot write \'%s\'[%d] of type %d to odb, db_set_data_index() status %d", name, index, tid, status);
          return -1;
        }

      return 0;
    }
  else
    {
      cm_msg(MERROR, frontend_name, "Cannot read \'%s\'[%d] from odb, db_find_key() status %d", name, index, status);
      return -1;
    }
};

int odbReadInt(const char*name,int index = 0,int defaultValue = 0)
{
  int value = defaultValue;
  if (odbReadAny(name,index,TID_INT,&value) == 0)
    return value;
  else
    return defaultValue;
};

uint32_t odbReadUint32(const char*name,int index = 0,uint32_t defaultValue = 0)
{
  uint32_t value = defaultValue;
  if (odbReadAny(name,index,TID_DWORD,&value) == 0)
    return value;
  else
    return defaultValue;
};

double odbReadDouble(const char*name,int index = 0,double defaultValue = 0)
{
  double value = defaultValue;
  if (odbReadAny(name,index,TID_DOUBLE,&value) == 0)
    return value;
  else
    return defaultValue;
};

float odbReadFloat(const char*name,int index = 0,double defaultValue = 0)
{
  float value = defaultValue;
  if (odbReadAny(name,index,TID_FLOAT,&value) == 0)
    return value;
  else
    return defaultValue;
};

bool     odbReadBool(const char*name,int index = 0,bool defaultValue = 0)
{
  uint32_t value = defaultValue;
  if (odbReadAny(name,index,TID_BOOL,&value) == 0)
    return value;
  else
    return defaultValue;
};

// const char* odbReadString(const char*name,int index,const char* defaultValue,int stringLength)
// {
//   const int bufSize = 512;
//   //char buf[bufSize];
//   char* buf = new char[bufSize];
//   buf[0] = 0;
//   if (defaultValue)
//     strlcpy(buf, defaultValue, bufSize);
//   assert(stringLength < bufSize);
//   if (odbReadAny(name, index, TID_STRING, buf, stringLength) == 0)
//     return buf;
//   else
//     return defaultValue;
// };

std::string odbReadString(const char*name,int index,const char* defaultValue, int stringLength)
{
  const int bufSize = 512;
  //char buf[bufSize];
  char* buf = new char[bufSize];
  //  buf[0] = 0;
  // if (defaultValue)
  //   strlcpy(buf, defaultValue, bufSize);
  //  printf("stringLength=%d, bufSize=%d\n",stringLength,bufSize);
  assert(stringLength < bufSize);
  std::string out="";
  if (odbReadAny(name, index, TID_STRING, buf, stringLength) == 0)
    out=buf;
    //return buf;
  else
    out=defaultValue;
    //    return defaultValue;
  delete buf;
  return out;
};

int odbReadArraySize(const char*name)
{
  int status;
  HNDLE hdir = 0;
  HNDLE hkey;
  KEY key;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status != SUCCESS)
    return 0;

  status = db_get_key(hDB, hkey, &key);
  if (status != SUCCESS)
    return 0;

  return key.num_values;
}

int odbResizeArray(const char*name, int tid, int size)
{
   int oldSize = odbReadArraySize(name);

   if (oldSize >= size)
      return oldSize;

   int status;
   HNDLE hkey;
   HNDLE hdir = 0;

   status = db_find_key (hDB, hdir, (char*)name, &hkey);
   if (status != SUCCESS)
      {
         cm_msg(MINFO, frontend_name, "Creating \'%s\'[%d] of type %d", name, size, tid);

         status = db_create_key(hDB, hdir, (char*)name, tid);
         if (status != SUCCESS)
            {
               cm_msg (MERROR, frontend_name, "Cannot create \'%s\' of type %d, db_create_key() status %d", name, tid, status);
               return -1;
            }
         
         status = db_find_key (hDB, hdir, (char*)name, &hkey);
         if (status != SUCCESS)
            {
               cm_msg(MERROR, frontend_name, "Cannot create \'%s\', db_find_key() status %d", name, status);
               return -1;
            }
      }
   
   cm_msg(MINFO, frontend_name, "Resizing \'%s\'[%d] of type %d, old size %d", name, size, tid, oldSize);

   status = db_set_num_values(hDB, hkey, size);
   if (status != SUCCESS)
      {
         cm_msg(MERROR, frontend_name, "Cannot resize \'%s\'[%d] of type %d, db_set_num_values() status %d", name, size, tid, status);
         return -1;
      }
   
   return size;
}

int odbWriteInt(const char*name,int index, int value)
{
  int status;
  HNDLE hdir = 0;
  HNDLE hkey;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      int size = sizeof(value);
      status = db_set_data_index(hDB, hkey, &value, size, index, TID_INT);
      if (status == SUCCESS)
	return 0;
    }

  return -1;
}

int odbWriteBool(const char*name,int index, BOOL value)
{
  int status;
  HNDLE hdir = 0;
  HNDLE hkey;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      int size = sizeof(int);
      status = db_set_data_index(hDB, hkey, &value, size, index, TID_BOOL);
      if (status == SUCCESS)
	return 0;
    }

  return -1;
}

int odbWriteDouble(const char*name,int index, double value)
{
  int status;
  HNDLE hdir = 0;
  HNDLE hkey;

  status = db_find_key (hDB, hdir, (char*)name, &hkey);
  if (status == SUCCESS)
    {
      //      int size = sizeof(value);
      int sss = sizeof(value);
      status = db_set_data_index(hDB, hkey, &value, sss, index, TID_DOUBLE);
      if (status == SUCCESS)
	return 0;
    }

  return -1;
}


// int set_equipment_status(const char *name, const char *equipment_status, const char *status_class)
// {
//    int status, idx;
//    char str[256];
//    HNDLE hKey;

//    for (idx = 0; equipment[idx].name[0]; idx++)
//       if (equal_ustring(equipment[idx].name, name))
//          break;

//    if (equal_ustring(equipment[idx].name, name)) {
//       sprintf(str, "/Equipment/%s/Common", name);
//       db_find_key(hDB, 0, str, &hKey);
//       assert(hKey);

//       status = db_set_value(hDB, hKey, "Status", equipment_status, 256, 1, TID_STRING);
//       assert(status == DB_SUCCESS);
//       status = db_set_value(hDB, hKey, "Status color", status_class, 32, 1, TID_STRING);
//       assert(status == DB_SUCCESS);
//    }

//    return SUCCESS;
// }

// end
