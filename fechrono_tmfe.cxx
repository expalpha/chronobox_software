/********************************************************************\

  Name:         fechrono_tmfe.cxx
  Created by:   AC
  Created by:   converted to tmfe, KO

  Contents:     Frontend for the ALPHA Chronobox

\********************************************************************/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>

#include "tmfe.h"
#include "cb.h"

#include "midas.h"

#include <deque>

#undef NDEBUG // midas required assert() to be always enabled

#include <stdio.h>
#include <math.h> // M_PI

#include "tmfe.h"

class EqCbFlow : // Translates scaler rates into calibrated water flow
                 public TMFeEquipment
{
public:
   Chronobox *fCb = NULL;
   const std::vector<double> *fScalersRate;
   const unsigned int fNflowChans = 8; // hardcoded in the firmware, so no point in making it changeable here
   const unsigned int fFlowChan = 40;  // same
   EqCbFlow(const char *eqname, const char *eqfilename, const std::vector<double> &fScalersRate_) // ctor
       : TMFeEquipment(eqname, eqfilename), fScalersRate(&fScalersRate_)
   {
      printf("EqCbFlow::ctor!\n");

      // configure the equipment here:

#if 0
    {"cbflow%02d",             /* equipment name */
     { 4,                      /* event ID */
       (1<<4),                 /* trigger mask */
       "",                     /* event buffer */
       EQ_PERIODIC,            /* equipment type */
       0,                      /* event source */
       "MIDAS",                /* format */
       TRUE,                   /* enabled */
       RO_ALWAYS,              /* when to read this event */
       1000,                   /* poll time in milliseconds */
       0,                      /* stop run after this event limit */
       0,                      /* number of sub events */
       1,                      /* whether to log history */
       "", "", "",},
     read_flow,                /* readout routine */
     NULL,
     NULL,
     NULL,                     /* bank list */
    },
#endif

      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 4;
      fEqConfTriggerMask = (1 << 4);
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;
      fEqConfReadOnlyWhenRunning = false;
      fEqConfEnablePoll = false; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
      //fEqConfWriteCacheSize = 0; // use default value
   }

   ~EqCbFlow() // dtor
   {
      printf("EqCbFlow::dtor!\n");
   }

   void HandleUsage()
   {
      printf("EqCbFlow::HandleUsage!\n");
   }

   TMFeResult HandleInit(const std::vector<std::string> &args)
   {
      printf("EqCbFlow::HandleInit!\n");
      if (!fCb)
         fCb = new Chronobox();
      return TMFeOk();
      double f = 0;
      fOdbEqVariables->RD("ColSumFlow", &f, true);
   }

   void HandlePeriodic()
   {
      //printf("EqCbFlow::HandlePeriodic!\n");

      if (fFe->fFeIndex != 1)
         return;

      static unsigned int count = 0;
      if(count < 5){
         std::cout << "Skipping first 5 events for rate to stabilize" << std::endl;
         count++;
      }
      if (fScalersRate->size() < fFlowChan + fNflowChans)
      {
         std::cerr << "fScalersRate has the wrong size: " << fScalersRate->size() << std::endl;
         return;
      }
      /* init bank structure */
      char event[102400];
      ComposeEvent(event, sizeof(event));
      BkInit(event, sizeof(event));
      double *p = (double *)BkOpen(event, "CBFL", TID_DOUBLE);

      double TotFlow = 0.;
      for (unsigned int i = 0; i < fNflowChans; i++)
      {
         double rate = (*fScalersRate)[fFlowChan + i]; // sample Rate in Hz
         p[i] = rate * 1.e-2 + 0.065;                  // https://daq.triumf.ca/elog-alphag/alphag/1797

         if (0)
            printf("flow ch: %d\trate: %1.3f Hz\tflow: %1.2f l/min\n",
                   i, rate, p[i]);
         TotFlow += p[i];
      }
      fOdbEqVariables->WD("ColSumFlow", TotFlow);

      //  p[gMcsClockChan] = (numClocks)*dt1;

      //Force bank to its historic size
      BkClose(event, p + fNflowChans);

      char stat[64];
      sprintf(stat, "Tot H20 Flow: %1.1f[l/min]", TotFlow);
      char col[64];
      if (TotFlow < 1.)
         sprintf(col, "yellow");
      else
         sprintf(col, "#00FF00");
      char eqname[10];
      sprintf(eqname, "cbflow%0d", fFe->fFeIndex);
      EqSetStatus(stat, col);

      printf("cbflow%0d  Tot H20 Flow: %1.1f[l/min]\n", fFe->fFeIndex, TotFlow);

      EqSendEvent(event);
   }
};

class SendEventPayload
{
public:
   char *event = NULL;
   size_t event_size = 0;
   bool write_to_odb = false;
};

class SendEvent
{
public:
   TMFeEquipment *fEq = NULL;

public:
   std::mutex fMutex;
   std::deque<SendEventPayload *> fQueue;
   std::thread *fThread = NULL;
   bool fThreadRunning = false;

public:
   uint32_t fQueueSize = 0;
   size_t fQueueBytes = 0;

public:
   void SendEventThread()
   {
      printf("SendEventThread started!\n");
      fThreadRunning = true;
      while (1)
      {
         fMutex.lock();
         if (fQueue.empty())
         {
            fMutex.unlock();
            TMFE::Sleep(0.001);
            continue;
         }
         SendEventPayload *p = fQueue.front();
         fQueue.pop_front();
         fQueueSize--;
         fQueueBytes -= p->event_size;
         fMutex.unlock();
         //printf("SendEvent size %d, queue %d events, %d bytes\n", p->event_size, (int)fQueueSize, (int)fQueueBytes);
         fEq->EqSendEvent(p->event, p->write_to_odb);
         free(p->event);
         p->event = NULL;
         delete p;
         //TMFE::Sleep(0.00001);
      }
   }

   void StartThread()
   {
      if (fThread)
         return;

      fThread = new std::thread(&SendEvent::SendEventThread, this);
   }

   void Send(char *event, size_t event_size, bool write_to_odb = true)
   {
      std::lock_guard<std::mutex> lock(fMutex);
      SendEventPayload *p = new SendEventPayload;
      p->event = event;
      p->event_size = event_size;
      p->write_to_odb = write_to_odb;
      fQueue.push_back(p);
      fQueueSize++;
      fQueueBytes += event_size;
   }

   void GetSize(uint32_t *qs, size_t *qb)
   {
      std::lock_guard<std::mutex> lock(fMutex);
      if (qs)
         *qs = fQueueSize;
      if (qb)
         *qb = fQueueBytes;
   }
};

class EqCb : public TMFeEquipment
{
public:
   Chronobox *fCb = NULL;
   uint32_t fCbFirmware = 0;
   uint32_t fCbNumInputs = 0;

public:
   std::vector<uint32_t> fScalersPrev;
   std::vector<uint32_t> fScalers;
   bool fInScalersPacket = false;
   size_t fNumScalers = 0;

   std::vector<double> fScalersEpoch;
   std::vector<double> fScalersSum;
   std::vector<double> fScalersRate;

   std::vector<uint32_t> fFifoData;

public:
   bool fFailed = false;
   time_t fSyncStartTime = 0;
   int fFifoMax = 0;
   int fFifoMaxEver = 0;

public:
   bool fEnableDlTdc = false;
   uint32_t fDlCtrl = 0;

public:
   SendEvent *fSendEvent = NULL;

public:
   std::mutex fMutex;

public:
   EqCb(const char *eqname, const char *eqfilename) // ctor
       : TMFeEquipment(eqname, eqfilename)
   {
      printf("EqCb::ctor!\n");

      fEqConfReadConfigFromOdb = false;
      fEqConfEventID = 4;
      fEqConfTriggerMask = (1 << 4);
      fEqConfPeriodMilliSec = 1000;
      fEqConfLogHistory = 1;
      fEqConfWriteEventsToOdb = true;
      fEqConfReadOnlyWhenRunning = false;
      fEqConfEnablePoll = true; // enable polled equipment
      //fEqConfPollSleepSec = 0; // to create a "100% CPU busy" polling loop, set poll sleep time to zero
      //fEqConfWriteCacheSize = 0; use default value

      fSendEvent = new SendEvent;
      fSendEvent->fEq = this;
   }

   ~EqCb() // dtor
   {
      printf("EqCb::dtor!\n");
   }

   void HandleUsage()
   {
      printf("EqCb::HandleUsage!\n");
   }

   TMFeResult InitChronobox(bool do_sync, bool do_reset)
   {
      std::lock_guard<std::mutex> lock(fMutex);
      fFailed = false;

      fCbFirmware = fCb->cb_read32(0);
      fCbNumInputs = fCb->cb_read32(15);

      fMfe->Msg(MINFO, "InitChronobox", "Chronobox firmware 0x%08x with %d inputs", fCbFirmware, fCbNumInputs);

      switch (fCbFirmware)
      {
      //case 0x61832695: break; // production build
      //case 0x62476e98: break; // quartus 21.1 test build
      //case 0x624e1905: break; // fast-to-slow test build
      //case 0x624e22ad: break; // production build
      case 0x62608957: break; // production build, fix first data word in fifo, more bits in ts overflow counter
      //case 0x64b37536: break; // DL TDC test
      //case 0x65443443: break; // DL TDC test, readout of 4 sipm boards
      //case 0x656930e3: break; // DL TDC test, readout of 4 sipm boards, A, B and T
      //case 0x65cafba3: break; // DL TDC fixed output mux
      //case 0x65dbdf18: break; // DL TDC add more channels, add TDC gate
      case 0x65dcdc8a: break; // DL TDC add more channels, add TDC gate
      case 0x65dd3f1e: break; // DL TDC with 35 channels
      case 0x65e295f0: break; // DL TDC with 35 channels and ABT signals.
      default:
         fMfe->Msg(MERROR, "InitChronobox", "Chronobox firmware 0x%08x is not compatible with this frontend", fCbFirmware);
         return TMFeErrorMessage(msprintf("Chronobox firmware 0x%08x is not compatible with this frontend", fCbFirmware));
      }

      fFifoMax = 0;
      fFifoMaxEver = 0;

      // setup the inputs

      {
         uint32_t inputs_invert_low = 0;
         uint32_t inputs_invert_high = 0;

         fOdbEqSettings->RU32("inputs_invert_low", &inputs_invert_low, true);
         fOdbEqSettings->RU32("inputs_invert_high", &inputs_invert_high, true);

         fCb->cb_write32(18, inputs_invert_low);
         fCb->cb_write32(19, inputs_invert_high);
      }

      // enable timestamp leading edge

      {
         uint32_t tsc_enable_le_low = 0xFFFFFFFF;  // enable all ECL
         uint32_t tsc_enable_le_high = 0x000000FF; // enable all LEMO

         fOdbEqSettings->RU32("tsc_enable_le_low", &tsc_enable_le_low, true);
         fOdbEqSettings->RU32("tsc_enable_le_high", &tsc_enable_le_high, true);

         fCb->cb_write32(20, tsc_enable_le_low);
         fCb->cb_write32(21, tsc_enable_le_high);
      }

      // enable timestamp trailing edge

      {
         uint32_t tsc_enable_te_low = 0xFFFFFFFF;  // enable all ECL
         uint32_t tsc_enable_te_high = 0x000000FF; // enable all LEMO

         fOdbEqSettings->RU32("tsc_enable_te_low", &tsc_enable_te_low, true);
         fOdbEqSettings->RU32("tsc_enable_te_high", &tsc_enable_te_high, true);

         fCb->cb_write32(22, tsc_enable_te_low);
         fCb->cb_write32(23, tsc_enable_te_high);
      }

      bool cb_reset = false;

      if (do_reset)
      {
         fMfe->Msg(MINFO, "InitChronobox", "Reset scalers and timestamp fifo!");

         // disable ext sync signal
         fCb->cb_write32(30, 0);
         fCb->cb_write32(31, 0);

         //fCb->cb_write32(32, 0x80000000);
         //fCb->cb_write32(32, 0);
         fCb->cb_arm_sync();

         cb_reset = true;
      }

      // disarm the sync circuit, just in case

      {
         fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x", fCb->cb_read32(32));

         //fCb->cb_write32(32, 0x40000000);
         //fCb->cb_write32(32, 0);
         fCb->cb_disarm_sync();

         fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x after disarm", fCb->cb_read32(32));
      }

      bool master = false;
      bool slave = false;
      bool chain = false;

      fOdbEqSettings->RB("clock_master", &master, true);
      fOdbEqSettings->RB("clock_slave", &slave, true);
      fOdbEqSettings->RB("clock_chain", &chain, true);

      if (slave || chain)
      {
         // enable ext sync signal LEMO 4 input
         fCb->cb_write32(30, 0);
         fCb->cb_write32(31, 0x10);
      }
      else
      {
         // disable ext sync signal
         fCb->cb_write32(30, 0);
         fCb->cb_write32(31, 0);
      }

      if (master || chain)
      {
         // enable clock (LEMO 0) and sync (LEMO 1) outputs
         //fCb->cb_write32(29, 0x00000021); // 2=cb_sync, 1=clk_ts
         fCb->cb_set_lemo_out_mux(0, 1);
         fCb->cb_set_lemo_out_mux(1, 2);
         fCb->cb_write32(13, 0x0F000000); // enable output tristates
      }
      else
      {
         // disable clock and sync outputs
         //fCb->cb_write32(29, 0x00000000);
         fCb->cb_set_lemo_out_mux(0, 0);
         fCb->cb_set_lemo_out_mux(1, 0);
         fCb->cb_write32(13, 0x00000000);
      }

#if 0   
      {
         int  sync_ext_input = -1;
         fOdbEqSettings->RI("sync_ext_input", &sync_ext_input, true);

         if (sync_ext_input < 0) {
            fCb->cb_write32(30, 0);
            fCb->cb_write32(31, 0);
         } else if (sync_ext_input < 32) {
            fCb->cb_write32(30, (1<<sync_ext_input));
            fCb->cb_write32(31, 0);
         } else if (sync_ext_input < 64) {
            fCb->cb_write32(30, 0);
            fCb->cb_write32(31, (1<<(sync_ext_input-32)));
         } else {
            fCb->cb_write32(30, 0);
            fCb->cb_write32(31, 0);
         }
      }
#endif

      if (master)
      {
         printf("configuring as clock master!\n");

         fCb->cb_int_clock();
      }
      else if (slave)
      {
         printf("configuring as clock slave!\n");

         fCb->cb_ext_clock();

         uint32_t pll_status = fCb->cb_read32(0x1C);

         //printf("pll_status 0x%08x\n", pll_status);

         if (pll_status != 0xC0000000)
         {
            fMfe->Msg(MERROR, "frontend_init", "Chronobox external clock fail, PLL status 0x%08x", pll_status);
         }
      }

      bool sync_failed = false;

      if (do_sync)
      {
         if (master || slave)
         {
            // arm the sync circuit
            //fCb->cb_write32(32, 0x80000000);
            //fCb->cb_write32(32, 0);
            fCb->cb_arm_sync();

            fSyncStartTime = time(NULL);

            uint32_t sync_status = fCb->cb_read32(32);

            if (sync_status == 0x6600)
            {
               fMfe->Msg(MERROR, "InitChronobox", "Invalid sync status 0x%08x after arm sync", sync_status);
               sync_failed = true;
            }
            else
            {
               fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x after arm sync", sync_status);
            }

            cb_reset = true;
         }

         if (master)
         {
            // generate sync signal
            fCb->cb_write32(32, 0x00010000);
            fCb->cb_write32(32, 0);
            fMfe->Msg(MINFO, "InitChronobox", "Generated master sync signal");
         }
      }

      uint32_t pll_status = fCb->cb_read32(0x1C);

      if (sync_failed)
      {
         EqSetStatus("sync failed", "#FF0000");
         fFailed = true;
      }
      else if (pll_status == 0x80000000)
      {
         EqSetStatus("internal clock", "#FFFF00");
      }
      else if (pll_status == 0xC0000000)
      {
         EqSetStatus("external clock", "#00FF00");
      }
      else if (pll_status == 0x60000000)
      {
         EqSetStatus("invalid external clock", "#FF0000");
         fFailed = true;
      }
      else if (pll_status == 0xA0000000)
      {
         if (master) {
            EqSetStatus("internal clock, no external clock", "#00FF00");
         } else {
            EqSetStatus("internal clock, no external clock", "#FF0000");
            fFailed = true;
         }
      }
      else
      {
         EqSetStatus("invalid pll state", "#FF0000");
         fFailed = true;
      }

      std::vector<std::string> names;

      for (int i = 0; i < 16; i++)
      {
         char buf[256];
         sprintf(buf, "p1_%d", i);
         names.push_back(buf);
      }
      for (int i = 0; i < 16; i++)
      {
         char buf[256];
         sprintf(buf, "p2_%d", i);
         names.push_back(buf);
      }
      for (int i = 0; i < 8; i++)
      {
         char buf[256];
         sprintf(buf, "lemo_%d", i);
         names.push_back(buf);
      }
      for (int i = 0; i < 18; i++)
      {
         char buf[256];
         sprintf(buf, "gpio_%d", i);
         names.push_back(buf);
      }
      names.push_back("ext_clock");
      names.push_back("sys_clock");

      assert(names.size() == fCbNumInputs + 1);

      fOdbEqSettings->RSA("names", &names, true, 32);

      if (cb_reset)
      {
         fScalersPrev.clear();
         fScalers.clear();
         fInScalersPacket = false;
         fNumScalers = 0;

         fScalersEpoch.clear();
         fScalersSum.clear();
         fScalersRate.clear();

         fFifoData.clear();

         SaveScalersLocked();
      }

      // configure the DL TDC and trigger

      {
         uint32_t dl_trg_mask   = 0; // disable all trigger inputs
         uint32_t dl_tdc_mask   = 0; // disable all tdc inputs

         fOdbEqSettings->RB("dl_enable",       &fEnableDlTdc, true);
         fOdbEqSettings->RU32("dl_trg_mask",   &dl_trg_mask, true);
         fOdbEqSettings->RU32("dl_tdc_mask",   &dl_tdc_mask, true);
         fOdbEqSettings->RU32("dl_ctrl",       &fDlCtrl, true);

         if (fEnableDlTdc) {
            fCb->cb_write32(36, dl_trg_mask);
            fCb->cb_write32(37, 0); // fDlCtrl
            fCb->cb_write32(38, dl_tdc_mask);

            fCb->cb_set_lemo_out_mux(1, 3); // dlA OR
            fCb->cb_set_lemo_out_mux(2, 4); // dlB OR
            fCb->cb_set_lemo_out_mux(3, 5); // dlT trigger
         }
      }

      return TMFeOk();
   }

   TMFeResult HandleInit(const std::vector<std::string> &args)
   {
      printf("EqCb::HandleInit!\n");
      if (!fCb)
         fCb = new Chronobox();

      int status = fCb->cb_open();

      if (status)
      {
         fMfe->Msg(MERROR, "HandleInit", "Cannot open chronobox, cb_open() returned %d", status);
         return TMFeErrorMessage("Chronobox Configuration FAILED");
      }

      TMFeResult r = InitChronobox(false, true);

      if (r.error_flag)
         return r;

      fSendEvent->StartThread();

      EqStartPollThread();

      return TMFeOk();
   }

   TMFeResult HandleBeginRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleBeginRun", "Begin run %d!", run_number);

      InitChronobox(true, false);

      if (fEnableDlTdc) {
         fCb->cb_write32(37, fDlCtrl);
      }

      return TMFeOk();
   }

   TMFeResult HandleEndRun(int run_number)
   {
      fMfe->Msg(MINFO, "HandleEndRun", "End run %d!", run_number);

      if (fEnableDlTdc) {
         fCb->cb_write32(37, 0); // fDlCtrl
      }

      if (!fFailed)
      {
         fCb->cb_latch_scalers();
         ReadFifoData();
      }

      fMutex.lock();
      SendFifoDataLocked();
      fMutex.unlock();

      return TMFeOk();
   }

   bool HandlePoll()
   {
      if (fFailed)
         return false;
      return true;
   }

   void SaveScalersLocked()
   {
      //printf("save scalers!\n");

      if (fScalers.empty())
      {
         fScalers.resize(fCbNumInputs + 1);
      }

      fScalersEpoch.resize(fScalers.size());
      fScalersSum.resize(fScalers.size());
      fScalersRate.resize(fScalers.size());

      for (size_t i = 0; i < fScalers.size(); i++)
      {
         if (fScalersPrev.size() > 0)
         {
            if (fScalers[i] < fScalersPrev[i])
            {
               // 32-bit overflow
               fScalersEpoch[i] += 1;
            }
            uint32_t incr = fScalers[i] - fScalersPrev[i];
            fScalersRate[i] = incr;
         }

         fScalersSum[i] = fScalers[i] + fScalersEpoch[i] * (double)0x100000000;
      }

      // double ext_clock_freq = 10e6; // 10 MHz timestamp clock
      double sys_clock_freq = 100e6; // 100 MHz timestamp clock

      double ext_clock_incr = fScalersRate[fCbNumInputs - 1];
      double sys_clock_incr = fScalersRate[fCbNumInputs];

      double dt = sys_clock_incr / sys_clock_freq;

      for (size_t i = 0; i < fScalers.size(); i++)
      {
         if (dt > 0)
         {
            fScalersRate[i] /= dt;
         }
         else
         {
            fScalersRate[i] = 0;
         }
      }

      double ext_clock = ext_clock_incr / dt;
      double sys_clock = sys_clock_incr / dt;

      uint32_t qs = 0;
      size_t qb = 0;

      fSendEvent->GetSize(&qs, &qb);

      printf("clock incr %12.0f cnt %12.1f Hz, ext %12.0f cnt %12.1f Hz, dt %6.3f, fifo %d/%d, send queue: %d events, %d bytes\n", sys_clock_incr, sys_clock, ext_clock_incr, ext_clock, dt, fFifoMax, fFifoMaxEver, (int)qs, (int)qb);

      fFifoMax = 0;

      if (qb > 100 * 1024 * 1024)
      {
         fMfe->Msg(MERROR, "SaveScalers", "chronobox ethernet overflow!");
         EqSetStatus("ethernet overflow", "#FF0000");
         fFailed = true;
      }

      if (1)
      {
         size_t event_size = 1024 + sizeof(double) * fScalersRate.size() + sizeof(double) * fScalersSum.size();
         char *event = (char *)malloc(event_size);
         ComposeEvent(event, event_size);
         BkInit(event, event_size);

         if (1)
         {
            char bankname[20];
            sprintf(bankname, "CBS%d", fFe->fFeIndex);
            size_t bksize = sizeof(double) * fScalersSum.size();
            char *p = (char *)BkOpen(event, bankname, TID_DOUBLE);
            memcpy(p, fScalersSum.data(), bksize);
            BkClose(event, p + bksize);
         }

         if (1)
         {
            char bankname[20];
            sprintf(bankname, "CBR%d", fFe->fFeIndex);
            size_t bksize = sizeof(double) * fScalersRate.size();
            char *p = (char *)BkOpen(event, bankname, TID_DOUBLE);
            memcpy(p, fScalersRate.data(), bksize);
            BkClose(event, p + bksize);
         }

         //EqSendEvent(event);
         //free(event);
         fSendEvent->Send(event, event_size);
      }

      fScalersPrev = fScalers;
      fScalers.clear();

      SendFifoDataLocked();
   }

   void SendFifoDataLocked()
   {
      if (fFifoData.empty())
      {
         return;
      }

      //printf("SendFifoData: %d words\n", fFifoData.size());

      size_t event_size = 1024 + sizeof(uint32_t) * fFifoData.size();
      char *event = (char *)malloc(event_size);
      ComposeEvent(event, event_size);
      BkInit(event, event_size);

      if (1)
      {
         char bankname[20];
         sprintf(bankname, "CBF%d", fFe->fFeIndex);
         size_t bksize = sizeof(uint32_t) * fFifoData.size();
         char *p = (char *)BkOpen(event, bankname, TID_DWORD);
         memcpy(p, fFifoData.data(), bksize);
         BkClose(event, p + bksize);
      }

      //EqSendEvent(event, false);
      //free(event);

      fSendEvent->Send(event, event_size, false);

      fFifoData.clear();
   }

   void ReadTdcData()
   {
      uint32_t sr = fCb->cb_read32(33);
      if (sr & 0x80000000)
         return;

      //printf("TDC status register: 0x%08x\n", sr);

      uint32_t nw = sr & 0xFFFF;

      size_t event_size = 1024 + sizeof(uint32_t) * nw * 2;
      char *event = (char *)malloc(event_size);
      ComposeEvent(event, event_size);
      BkInit(event, event_size);

      char bankname[20];
      sprintf(bankname, "CBT%d", fFe->fFeIndex);
      uint32_t *p32 = (uint32_t*) BkOpen(event, bankname, TID_DWORD);

      for (uint32_t i=0; i<nw; i++) {
         uint32_t lo = fCb->cb_read32(34);
         uint32_t hi = fCb->cb_read32(35);
         //printf("%2d: 0x%08x 0x%08x\n", i, hi, lo);

         *p32++ = lo;
         *p32++ = hi;
      }

      BkClose(event, p32);

      //EqSendEvent(event, false);
      //free(event);

      fSendEvent->Send(event, event_size, false);
   }

   void ReadFifoData()
   {
      if (fFailed)
         return;

      std::lock_guard<std::mutex> lock(fMutex);

      if (fEnableDlTdc) {
         ReadTdcData();
      }

      bool verbose = false;

      std::vector<uint32_t> fifo_data;
      bool overflow = false;

      fCb->cb_read_fifo(&fifo_data, &overflow);

      if (overflow)
      {
         fMfe->Msg(MERROR, "HandlePollRead", "chronobox fifo overflow!");
         EqSetStatus("fifo overflow", "#FF0000");
         fFailed = true;
         return;
      }

      int nread = fifo_data.size();

      if (nread == 0)
      {
         return;
      }

      if (verbose)
      {
         printf("read %d words, overflow %d\n", nread, overflow);
      }

      if (nread > fFifoMax)
         fFifoMax = nread;

      if (fFifoMax > fFifoMaxEver)
         fFifoMaxEver = fFifoMax;

      if (fifo_data.size() > 0)
      {
         size_t n32 = fifo_data.size();

         for (size_t i = 0; i < n32; i++)
         {
            uint32_t v = fifo_data[i];

            fFifoData.push_back(v);

            if (verbose)
            {
               //printf("read %3d: 0x%08x", i, v);
               printf("read %3d: 0x%08x (%d)", (int)i, v, v);
            }

            if (fInScalersPacket)
            {
               if (verbose)
               {
                  printf(" scaler %d", (int)fScalers.size());
               }

               fScalers.push_back(v);

               if (fScalers.size() == fNumScalers)
               {
                  fInScalersPacket = false;
                  fNumScalers = 0;
                  if (verbose)
                  {
                     printf(" (last)");
                  }
                  SaveScalersLocked();
               }
            }
            else if ((v & 0xFF000000) == 0xFF000000)
            {
               if (verbose)
               {
                  printf(" overflow 0x%04x", v & 0xFFFF);
               }
            }
            else if ((v & 0xFF000000) == 0xFE000000)
            {
               fNumScalers = v & 0xFFFF;
               fInScalersPacket = true;
               if (verbose)
               {
                  printf(" packet of %d scalers", (int)fNumScalers);
               }

               if (fNumScalers != fCbNumInputs + 1)
               {
                  fMfe->Msg(MERROR, "HandlePollRead", "unexpected number of scalers %d vs expected %d!", (int)fNumScalers, fCbNumInputs + 1);
                  EqSetStatus("wrong number of scalers in fifo", "#FF0000");
                  fFailed = true;
                  return;
               }
            }
            else
            {
               //uint32_t ts = v & 0x00FFFFFF;
               //int ch = (v & 0x7F000000)>>24;
            }
            if (verbose)
            {
               printf("\n");
            }
         }
      }

      if (fFifoData.size() > 50000)
      {
         SendFifoDataLocked();
      }

#if 0
      if (fifo_data.size() > 0) {
         size_t event_size = 1024+sizeof(uint32_t)*fifo_data.size();
         char* event = (char*)malloc(event_size);
         ComposeEvent(event, event_size);
         BkInit(event, event_size);

         if (1) {
            char bankname[20];
            sprintf(bankname,"CBF%d", fFe->fFeIndex);
            /* init bank structure */
            size_t bksize = sizeof(uint32_t)*fifo_data.size();
            char* p = (char*)BkOpen(event, bankname, TID_DWORD);
            memcpy(p, fifo_data.data(), bksize);
            BkClose(event, p + bksize);
         }

         EqSendEvent(event, false);
         free(event);
      }
#endif
   }

   void HandlePollRead()
   {
      if (fFailed)
         return;

      ReadFifoData();
   }

   void HandlePeriodic()
   {
      //printf("EqCb::HandlePeriodic!\n");

      if (fFailed)
         return;

      if (fSyncStartTime)
      {
         time_t now = time(NULL);
         int dt = now - fSyncStartTime;
         uint32_t reg32 = fCb->cb_read32(32);
         if ((reg32 & 0xffff) == 0x6600)
         {
            fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x: sync success after %d sec", reg32, dt);
            fSyncStartTime = 0;
            EqSetStatus("sync ok", "#00FF00");
         }
         else if ((reg32 & 0xffff) == 0x8800)
         {
            if (dt > 30)
            {
               fMfe->Msg(MERROR, "InitChronobox", "Sync status 0x%08x: sync timeout after %d sec", reg32, dt);
               fSyncStartTime = 0;
               //fCb->cb_write32(32, 0x40000000);
               //fCb->cb_write32(32, 0);
               //fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x after disarm", fCb->cb_read32(32));
               EqSetStatus("sync timeout", "#FF0000");
               fFailed = true;
            }
            else
            {
               fMfe->Msg(MINFO, "InitChronobox", "Sync status 0x%08x: waiting for sync, %d sec", reg32, dt);
               EqSetStatus("waiting for sync...", "#FFFF00");
               return;
            }
         }
         else
         {
            fMfe->Msg(MERROR, "InitChronobox", "Sync status 0x%08x: unexpected sync status after %d sec", reg32, dt);
            fSyncStartTime = 0;
            EqSetStatus("unexpected sync status", "#FF0000");
            fFailed = true;
         }
      }

      fCb->cb_latch_scalers();
   }
};

// example frontend

class FeChrono : public TMFrontend
{
public:
   Chronobox *fCb = NULL;
   FeChrono() // ctor
   {
      printf("FeChrono::ctor!\n");
      FeSetName("fechrono%02d");
      fCb = new Chronobox;
   }

   void HandleUsage()
   {
      printf("FeChrono::HandleUsage!\n");
   };

   TMFeResult HandleArguments(const std::vector<std::string> &args)
   {
      printf("FeChrono::HandleArguments!\n");
      return TMFeOk();
   };

   TMFeResult HandleFrontendInit(const std::vector<std::string> &args)
   {
      printf("FeChrono::HandleFrontendInit!\n");
      EqCb *eqcb = new EqCb("cb%02d", __FILE__);
      eqcb->fCb = fCb;
      FeAddEquipment(eqcb);

      if (fFeIndex == 1)   // Only create flow equipment for cb01, hardcoded because it goes along with firmware mods
      {
         printf("Creating cbflow\n");
         fMfe->Msg(MINFO, "FeChrono", "Creating cbflow for cb %d", fFeIndex);
         EqCbFlow *eqcbflow = new EqCbFlow("cbflow%02d", __FILE__, eqcb->fScalersRate);
         eqcbflow->fCb = fCb;
         FeAddEquipment(eqcbflow);
      } else {
         fMfe->Msg(MINFO, "FeChrono", "No cbflow for cb %d", fFeIndex);
      }

      return TMFeOk();
   };

   TMFeResult HandleFrontendReady(const std::vector<std::string> &args)
   {
      printf("FeChrono::HandleFrontendReady!\n");
      //fMfe->StartPeriodicThread();
      //fMfe->StartRpcThread();
      return TMFeOk();
   };

   void HandleFrontendExit()
   {
      printf("FeChrono::HandleFrontendExit!\n");
   };
};

// boilerplate main function

int main(int argc, char *argv[])
{
   FeChrono fe_chrono;
   return fe_chrono.FeMain(argc, argv);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
