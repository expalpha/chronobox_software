#!/usr/bin/perl -w

if (grep(/--off/, @ARGV)) {
    i2c_off();
    bias_off();
    all_off();
    exit(0);
}

if (grep(/--on/, @ARGV)) {
    i2c_off();
    on_0F();
    exit(0);
}

if (grep(/--0/, @ARGV)) {
    i2c_off();
    exit(0);
}

if (grep(/--1/, @ARGV)) {
    i2c_enable(0x00,0x01);
    exit(0);
}

if (grep(/--2/, @ARGV)) {
    i2c_enable(0x00,0x02);
    exit(0);
}

if (grep(/--3/, @ARGV)) {
    i2c_enable(0x00,0x04);
    exit(0);
}

if (grep(/--4/, @ARGV)) {
    i2c_enable(0x00,0x08);
    exit(0);
}

init_dldb();

if (grep(/--temp/, @ARGV)) {
    read_temp("0x49");
    read_temp("0x4B");
    exit(0);
}

if (grep(/--bias-all-on/, @ARGV)) {
    bias_all_on();
    exit(0);
}

if (grep(/--bias-all-off/, @ARGV)) {
    bias_off();
    exit(0);
}

#all_on();
#on_0F();
#all_off();

#on_01();
#i2c_01();

#die "Here!";

#all_off();
#i2c_off();
#bias_off();

while (1) {
    my $vbias = read_dldb_vbias18();
    my $ibias = read_dldb_ibias18();
    my $t1 = read_temp_chan("0x49",0);
    my $t2 = read_temp_chan("0x49",1);
    my $t3 = read_temp_chan("0x49",4);
    my $t4 = read_temp_chan("0x49",5);
    #read_temp("0x4B");
    printf("temp %.3f %.3f %.3f %.3f C, vbias %.4f V, ibias %.7f A\n", $t1, $t2, $t3, $t4, $vbias, $ibias);
    sleep(1);
}

sub init_dldb
{
    system "i2ctransfer -y 1 w1\@0x49 0x84"; # U19 temp adc
    system "i2ctransfer -y 1 w1\@0x4B 0x84"; # U20 temp adc

    system "i2ctransfer -y 1 w3\@0x48 144 80 0"; # U1 vbias dac
    system "i2ctransfer -y 1 w3\@0x4A 144 80 0"; # U2 vbias dac
}

sub all_off
{
    print "all_off!\n";
    
    system "i2ctransfer -y 1 w2\@0x21 0x06 0x00"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x07 0x00"; # U5 power enable

    system "i2ctransfer -y 1 w2\@0x21 0x02 0x00"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x03 0x00"; # U5 power enable
}

sub all_on
{
    print "all_on!\n";
    
    system "i2ctransfer -y 1 w2\@0x21 0x06 0x00"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x07 0x00"; # U5 power enable

    system "i2ctransfer -y 1 w2\@0x21 0x02 0xFF"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x03 0xFF"; # U5 power enable
}

sub on_01
{
    print "on_01!\n";

    system "i2ctransfer -y 1 w2\@0x21 0x06 0x00"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x07 0x00"; # U5 power enable
    
    system "i2ctransfer -y 1 w2\@0x21 0x02 0x01"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x03 0x00"; # U5 power enable
}

sub on_0F
{
    print "on_0F!\n";

    system "i2ctransfer -y 1 w2\@0x21 0x06 0x00"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x07 0x00"; # U5 power enable
    
    system "i2ctransfer -y 1 w2\@0x21 0x02 0x0F"; # U5 power enable
    system "i2ctransfer -y 1 w2\@0x21 0x03 0x00"; # U5 power enable
}

sub i2c_off
{
    print "i2c_off!\n";

    system "i2ctransfer -y 1 w2\@0x26 0x06 0x00"; # U6 i2c enable
    system "i2ctransfer -y 1 w2\@0x26 0x07 0x00"; # U6 i2c enable

    system "i2ctransfer -y 1 w2\@0x26 0x02 0x00"; # U6 i2c enable
    system "i2ctransfer -y 1 w2\@0x26 0x03 0x00"; # U6 i2c enable
}

sub i2c_enable
{
    my $high = shift @_;
    my $low  = shift @_;

    print "i2c_enable $high $low!\n";

    system "i2ctransfer -y 1 w2\@0x26 0x06 0x00"; # U6 i2c enable
    system "i2ctransfer -y 1 w2\@0x26 0x07 0x00"; # U6 i2c enable

    system "i2ctransfer -y 1 w2\@0x26 0x02 $low"; # U6 i2c enable
    system "i2ctransfer -y 1 w2\@0x26 0x03 $high"; # U6 i2c enable
}

sub bias_off
{
    print "bias_off!\n";

    system "i2ctransfer -y 1 w3\@0x48 15 0x00 0x00";
    system "i2ctransfer -y 1 w3\@0x4A 15 0x00 0x00";
}

sub bias_all_on
{
    print "bias_all_on!\n";

    system "i2ctransfer -y 1 w3\@0x48 15 0xD0 0x00";
    system "i2ctransfer -y 1 w3\@0x4A 15 0xD0 0x00";
}

sub read_dldb_vbias
{
    system "i2ctransfer -y 1 w1\@0x69 0x30";
    my $data = `i2ctransfer -y 1 r3\@0x69`;
    chop $data;
    my @data = split(/\s+/,$data);
    my $raw = 256*hex($data[0]) + hex($data[1]);
    my $cnf = hex($data[2]);
    my $v = $raw/1000.0; # conert it to Volts from mV
    my $R5=1000000.0;
    my $R6=20000.0;
    my $hv_raw = $v/$R6 * ($R5+$R6); # Recalculate HV at the input of R divider 1Mohm/20kohm
    my $hv = $hv_raw * 0.9177;
    printf "DL-DB vbias: $data - $raw - $cnf - $v - $hv_raw - $hv\n";
    return $hv;
}

sub read_dldb_vbias18
{
    system "i2ctransfer -y 1 w1\@0x69 0x3C";
    for (my $loop=0; $loop<30; $loop++) {
      my $data = `i2ctransfer -y 1 r4\@0x69`;
      chop $data;
      my @data = split(/\s+/,$data);
      my $raw = 256*256*hex($data[0]) + 256*hex($data[1]) + hex($data[2]);
      my $cnf = hex($data[3]);
      if ($cnf & 0x80) {
	next;
      }
      my $v = $raw*15.625*1e-6; # 15.625 μV
      my $R5=1000000.0;
      my $R6=20000.0;
      my $hv_raw = $v/$R6 * ($R5+$R6); # Recalculate HV at the input of R divider 1Mohm/20kohm
      my $hv = $hv_raw * 0.9177;
      #printf "DL-DB vbias: $data - $loop - $raw - $cnf - $v - $hv_raw - $hv\n";
      return $hv;
    }
    return 0;
}

sub read_dldb_ibias
{
    system "i2ctransfer -y 1 w1\@0x69 0x10";
    my $data = `i2ctransfer -y 1 r3\@0x69`;
    chop $data;
    my @data = split(/\s+/,$data);
    my $raw = 256*hex($data[0]) + hex($data[1]);
    my $cnf = hex($data[2]);
    my $v = $raw/1000.0; # conert it to Volts from mV
    my $i = $v/60.0/0.5; # 60 is opamp gain, 0.5 is the shunt resistor
    printf "DL-DB ibias: $data - $raw - $cnf - $v - $i\n";
    return $i;
}

sub read_dldb_ibias18
{
    system "i2ctransfer -y 1 w1\@0x69 0x1C";
    for (my $loop=0; $loop<30; $loop++) {
      my $data = `i2ctransfer -y 1 r4\@0x69`;
      chop $data;
      my @data = split(/\s+/,$data);
      my $raw = 256*256*hex($data[0]) + 256*hex($data[1]) + hex($data[2]);
      my $cnf = hex($data[3]);
      if ($cnf & 0x80) {
	next;
      }
      my $v = $raw*15.625*1e-6; # 15.625 μV
      my $i = $v/60.0/0.5; # 60 is opamp gain, 0.5 is the shunt resistor
      #printf "DL-DB ibias: $data - $loop - $raw - $cnf - $v - $i\n";
      return $i;
    }
    return 0;
}

sub read_temp
{
    my $addr = shift @_;

    printf("ADDR - CHAN - DATA   - RAW  - RT       - TCH\n");

    for (my $chan=0; $chan<8; $chan++) {
	my $command = 0x84 + $chan*16;
	system "i2ctransfer -y 1 w1\@$addr $command";
	my $data = `i2ctransfer -y 1 r2\@$addr`;
	chop $data;
	my @data = split(/\s+/,$data);
	my $raw = 256*hex($data[0]) + hex($data[1]);
	if ($raw == 4095) {
	    printf "$addr - $chan - $data - $raw - n/c\n";
	} else {
	    my $R25 = 5000.0; # R25 = 5kohm
	    my $RT = $raw*$R25/(4096-$raw);
	    my $TCH = 28.54 * pow3($RT/$R25) - 158.5 * pow2($RT/$R25) + 474.8 * ($RT/$R25) - 319.85; # in degC
	    printf "$addr - $chan - $data - $raw - $RT - $TCH degC\n";
	}
    }
}

sub read_temp_chan
{
    my $addr = shift @_;
    my $chan = shift @_;

    my $command = 0x84 + $chan*16;
    system "i2ctransfer -y 1 w1\@$addr $command";
    my $data = `i2ctransfer -y 1 r2\@$addr`;
    chop $data;
    my @data = split(/\s+/,$data);
    my $raw = 256*hex($data[0]) + hex($data[1]);
    if ($raw == 4095) {
	return -1.0;
    } else {
	my $R25 = 5000.0; # R25 = 5kohm
	my $RT = $raw*$R25/(4096-$raw);
	my $TCH = 28.54 * pow3($RT/$R25) - 158.5 * pow2($RT/$R25) + 474.8 * ($RT/$R25) - 319.85; # in degC
	return $TCH;
    }
}

sub pow2
{
    my $v = shift @_;
    return $v*$v;
}

sub pow3
{
    my $v = shift @_;
    return $v*$v*$v;
}

#system "i2ctransfer -y 1 w1\@0x6b 0x1C";
#system "i2ctransfer -y 1 w1\@0x6b 0x0C";

#while (1) {
#    my $data = `i2ctransfer -y 1 r6\@0x6b`;
#    chop $data;
#    my @data = split(/\s+/,$data);
#    my $raw = 256*256*hex($data[0]) + 256*hex($data[1]) + hex($data[2]);
#    my $volt= $raw*15.625*1e-6; # 15.625 μV
#    my $sta = $data[3];
#    my $sta2 = $data[4];
#    my $good = ($sta ne $sta2);
#    print "$data raw $raw status $sta $sta2 good $good volt $volt\n";
#    if ($good) {
#	sleep(1);
#    }
#}

#end
