#ifndef _STORE_CB_
#define _STORE_CB_


#include "TObject.h"
#include "unpack_cb.h"

#include "ChronoUtil.h"
#include "TChronoChannel.h"

class TCbFIFOEvent: public TObject
{
   public:
      double   fTime;
      uint32_t fEpoch;
      uint32_t fTimestamp;
      uint32_t fChannel;
      uint32_t fFlags;
      std::string fBoard;
      uint32_t fCounts;
      int fRunNumber;
      uint32_t fMidasTime;
   public:
  
   TCbFIFOEvent();
   TCbFIFOEvent(const TCbFIFOEvent& e);
   TCbFIFOEvent(const CbHit& hit, const std::string& board, int run_number);
   TCbFIFOEvent& operator+=(const TCbFIFOEvent& e);
   bool operator==(const TChronoChannel ch) const
   {
      if (fChannel != (uint32_t)ch.GetChannel())
         return false;
      if (fBoard == ch.GetBoard())
         return true;
      return false;
   }
   virtual ~TCbFIFOEvent();
   using TObject::Print;
   virtual void Print()
   {
      std::cout<<"Board: "<< fBoard << "\tChannel: " << fChannel<<std::endl;
      std::cout<<"RunTime: " << fTime<<"\t";
      if (IsLeadingEdge())
         std::cout<<"LE";
      else
         std::cout <<"TE";
      std::cout << std::endl;
   }
   void Reset();

   void SetRunNumber(const int r) { fRunNumber = r; }
   int GetRunNumber() const { return fRunNumber; }

   uint32_t GetMidasTime() const { return fMidasTime; }

   int GetScalerModule() const
   {
      if ( fBoard.size() )
         return TChronoChannel::CBMAP.at(fBoard) * CHRONO_N_CHANNELS + fChannel;
      else
         return -1;
   }
   void SetScalerModuleNo(int i) {
      fChannel = i % CHRONO_N_CHANNELS;
      int MAP_ENTRY = floor ( i / CHRONO_N_CHANNELS);
      for (const std::pair<std::string,int> board: TChronoChannel::CBMAP)
      {
          if (board.second == MAP_ENTRY)
         {
            fBoard = board.first;
            return;
         }
      }
      fBoard = "";
   }
   double GetRunTime() const     { return fTime;      }
   uint32_t GetEpoch() const     { return fEpoch;     }
   uint32_t GetTimestamp() const { return fTimestamp; }
   uint32_t GetChannel() const   { return fChannel;   }
   uint32_t GetFlag() const      { return fFlags;     }

   bool IsLeadingEdge() const    { return ! (fFlags & CB_HIT_FLAG_TE); }
   bool IsFallingEdge() const    { return fFlags & CB_HIT_FLAG_TE; }


    
  ClassDef(TCbFIFOEvent,1)
};

#endif
