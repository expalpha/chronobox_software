# Makefile for chronobox software. K.Olchanski TRIUMF August 2018
#
# to build MIDAS for the chronobox:
#
# ssh agdaq@cb02
# cd ~/packages/midas
# make remoteonly
# ls -l linux-armv7l-remoteonly/lib/libmidas.a
#

ARCH := $(shell uname -m)

#MIDASSYS := $(HOME)/packages/midas-develop
#MIDASSYS := $(HOME)/git/midas

LIB_DIR   = $(MIDASSYS)/linux-$(ARCH)-remoteonly/lib

CFLAGS  += -std=c++11 -Wall -Wuninitialized -g -Ialtera -Dsoc_cv_av -I$(MIDASSYS)/include -I$(MIDASSYS)/mvodb
LDFLAGS += -lm -lz -lutil -lnsl -lpthread -lrt

LIB = $(LIB_DIR)/libmidas.a

EXES += srunner_cb.exe
EXES += reboot_cb.exe
EXES += test_cb.exe
EXES += fechrono_tmfe.exe
EXES += fedldb.exe

all:: $(EXES)

clean:
	rm -f $(EXES) *.o

cb02:
	ssh agdaq@cb02 "bash -l -c \"make -C online/chronobox_software\""

%.o: %.cxx
	$(CXX) $(CFLAGS) -c $<

reboot_cb.exe: reboot_cb.o cb.o
	$(CXX) -o $@ $(CFLAGS) $^

test_cb.exe: test_cb.o cb.o
	$(CXX) -o $@ $(CFLAGS) $^

#test_unpack.exe: test_unpack.o cb.o unpack_cb.o
#	$(CXX) -o $@ $(CFLAGS) $^

#test_cb_ac.exe: test_cb_ac.o cb.o
#	$(CXX) -o $@ $(CFLAGS) $^

srunner_cb.exe: srunner_cb.o cb.o
	$(CXX) -o $@ $(CFLAGS) $^

fechrono_tmfe.exe: fechrono_tmfe.o cb.o
	$(CXX) -o $@ $(CFLAGS) $^ $(LIB) $(LDFLAGS)

fedldb.exe: fedldb.o koi2c.o
	$(CXX) -o $@ $(CFLAGS) $^ $(LIB) $(LDFLAGS) -li2c

#end
