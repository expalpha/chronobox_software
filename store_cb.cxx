#include "store_cb.h"


  
TCbFIFOEvent::TCbFIFOEvent():
    TObject(),
    fTime(-1),
    fEpoch(0),
    fTimestamp(0),
    fChannel(-1),
    fFlags(-1),
    fBoard(""),
    fCounts(0),
    fRunNumber(-1),
    fMidasTime(0)
{
}

TCbFIFOEvent::TCbFIFOEvent(const TCbFIFOEvent& e): 
    TObject(e),
    fTime(e.fTime),
    fEpoch(e.fEpoch),
    fTimestamp(e.fTimestamp),
    fChannel(e.fChannel),
    fFlags(e.fFlags),
    fBoard(e.fBoard),
    fCounts(e.fCounts),
    fRunNumber(e.fRunNumber),
    fMidasTime(e.fMidasTime)
{
}

TCbFIFOEvent& TCbFIFOEvent::operator+=(const TCbFIFOEvent& e)
{
   assert(fBoard == e.fBoard);
   assert(fChannel == e.fChannel);
   assert(fRunNumber == e.fRunNumber);
   if (fTimestamp == 0 )
      fTimestamp = e.fTimestamp;
   if (e.IsLeadingEdge())
      fCounts += e.fCounts;
   if (fMidasTime == 0 )
      fMidasTime = e.fMidasTime;
   return *this;
}

TCbFIFOEvent::~TCbFIFOEvent()
{

}

void TCbFIFOEvent::Reset()
{
    fTime = -1;
    fEpoch = 0;
    fTimestamp = 0;
    fChannel = -1;
    fFlags = -1;
    fBoard = "";
    fCounts = 0;
    fRunNumber = -1;
    fMidasTime = 0;
}

TCbFIFOEvent::TCbFIFOEvent(const CbHit& hit, const std::string& board, int run_number)
{
    fTime      = hit.time;
    fEpoch     = hit.epoch;
    fTimestamp = hit.timestamp;
    fChannel   = hit.channel;
    fFlags     = hit.flags;
    fBoard     = board;
    if (!(fFlags & CB_HIT_FLAG_TE))
       fCounts = 1;
    else
       fCounts = 0;
    fRunNumber = run_number;
    fMidasTime = hit.midastime;
}


ClassImp(TCbFIFOEvent)
