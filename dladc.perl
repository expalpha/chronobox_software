#!/usr/bin/perl -w

system "i2ctransfer -y 1 w1\@0x6b 0x1C";
#system "i2ctransfer -y 1 w1\@0x6b 0x0C";

while (1) {
    my $data = `i2ctransfer -y 1 r6\@0x6b`;
    chop $data;
    my @data = split(/\s+/,$data);
    my $raw = 256*256*hex($data[0]) + 256*hex($data[1]) + hex($data[2]);
    my $volt= $raw*15.625*1e-6; # 15.625 μV
    my $sta = $data[3];
    my $sta2 = $data[4];
    my $good = ($sta ne $sta2);
    my $R33 = 1000000.0;
    my $R34 =   20000.0;
    my $hv = $volt/($R34/($R33+$R34));
    print "$data raw $raw status $sta $sta2 good $good volt $volt hv $hv\n";
    if ($good) {
	sleep(1);
    }
}

#end
