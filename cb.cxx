// cb.cxx --- chronobox driver K.Olchanski TRIUMF Aug 2018

#undef NDEBUG // this program requires working assert()

#include "cb.h"

#include <stdio.h>
#include <string.h> // memcpy()
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <assert.h> // assert()
#include "hwlib.h"
#include "socal/socal.h"
#include "socal/hps.h"
#include "socal/alt_gpio.h"
#include "hps_0.h"

#define HW_REGS_BASE ( ALT_STM_OFST )
#define HW_REGS_SPAN ( 0x04000000 )
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )

// per https://www.intel.com/content/www/us/en/programmable/hps/cyclone-v/hps.html#sfo1418687413697.html

#define LWFPGASLAVES   0xFF200000
#define HPS2FPGASLAVES 0xC0000000 

#define H2F_BASE 0xC0000000 
#define H2F_SIZE 0x00010000

Chronobox::Chronobox() // ctor
{

}

Chronobox::~Chronobox() // dtor
{
   fBase = NULL;
}

int Chronobox::cb_open()
{
   char *virtual_base;
   int fd;
   
   // map the address space for the LED registers into user space so we can interact with them.
   // we'll actually map in the entire CSR span of the HPS since we want to access various registers within that span
   
   if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
      printf( "ERROR: could not open \"/dev/mem\"...\n" );
      return 1;
   }

   //printf("Chronobox ALT_HPS_ADDR 0x%08x\n", ALT_HPS_ADDR);
   printf("Chronobox ALT_STM_OFST        0x%08x mmap offset 0x%08x\n", ALT_STM_OFST,        ALT_STM_OFST&HW_REGS_MASK);
   printf("Chronobox ALT_LWFPGASLVS_OFST 0x%08x mmap offset 0x%08x\n", ALT_LWFPGASLVS_OFST, ALT_LWFPGASLVS_OFST&HW_REGS_MASK);
   printf("Chronobox ALT_H2F_OFST        0x%08x mmap offset 0x%08x\n", ALT_H2F_OFST,        ALT_H2F_OFST&HW_REGS_MASK);

   printf("Chronobox mapping /dev/mem at 0x%08x+0x%08x\n", HW_REGS_BASE, HW_REGS_SPAN);
   
   virtual_base = (char*)mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );
   
   if( virtual_base == MAP_FAILED ) {
      printf( "ERROR: mmap() failed...\n" );
      close( fd );
      return 1;
   }

   fBase = virtual_base;

   printf("Chronobox mapping /dev/mem at 0x%08x+0x%08x\n", H2F_BASE, H2F_SIZE);
   
   virtual_base = (char*)mmap( NULL, H2F_SIZE, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, H2F_BASE );
   
   if( virtual_base == MAP_FAILED ) {
      printf( "ERROR: mmap() failed...\n" );
      close( fd );
      return 1;
   }

   fBaseH2F = virtual_base;

   // file descriptor not needed after mmap()
   close(fd);
   
   printf("Chronobox SYSID_QSYS_BASE at 0x%08x\n", SYSID_QSYS_BASE);
   printf("Chronobox reading QSYS registers...");

   uint32_t system_id = fpga_read32(SYSID_QSYS_BASE);
   uint32_t system_timestamp = fpga_read32(SYSID_QSYS_BASE + 4);

   printf("\n\nChronobox SYSID_QSYS reg0: 0x%08x (system ID)\n", system_id);
   printf("Chronobox SYSID_QSYS reg1: 0x%08x (system timestamp)\n", system_timestamp);
   printf("\n");

   if (system_id != 0xf00f1302) {
      printf( "ERROR: Unexpected system ID 0x%08x\n", system_id);
      close( fd );
      return 1;
   }

   if (system_timestamp >= 0x60860b20) {
      fVersion = 2;
   } else if (system_timestamp >= 0x60736ab6) {
      fVersion = 1;
   } else {
      fVersion = 0;
   }

   if (fVersion == 0) {
      printf("Chronobox read_data:  0x%08x\n", fpga_read32(0x00+0));
      printf("Chronobox write_data: 0x%08x\n", fpga_read32(0x10+0));
      printf("Chronobox addr:       0x%08x\n", fpga_read32(0x20+0));
      printf("\n");
   }

   printf("Chronobox firmware revision: 0x%08x, register access version %d\n", cb_read32(0), fVersion);
   printf("\n");

   return 0;
}

uint32_t Chronobox::fpga_read32(int addr)
{
   void* laddr = fBase + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + addr ) & ( unsigned long)( HW_REGS_MASK ) );
  // write *(uint32_t *)h2p_lw_led_addr = ~led_mask; 

   return *(uint32_t *)laddr;
}

void Chronobox::fpga_write32(int addr, uint32_t data)
{
   void* laddr = fBase + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + addr ) & ( unsigned long)( HW_REGS_MASK ) );
   *(uint32_t *)laddr = data;
}

char* Chronobox::fpga_addr(int addr)
{
   return (char*)(fBase + ( ( unsigned long  )( ALT_LWFPGASLVS_OFST + addr ) & ( unsigned long)( HW_REGS_MASK ) ));
}

uint64_t Chronobox::h2f_read64(int addr)
{
   void* laddr = fBaseH2F + addr;
   return *(uint64_t *)laddr;
}

void Chronobox::h2f_write64(int addr, uint64_t data)
{
   void* laddr = fBaseH2F + addr;
   *(uint64_t *)laddr = data;
}

char* Chronobox::h2f_addr(int addr)
{
   return (char*)(fBaseH2F + addr);
}

uint32_t Chronobox::cb_read32(int ireg)
{
   if (fVersion == 0) {
      fpga_write32(0x20+0, 0x40000000 + ireg);
      return fpga_read32(0x00+0);
   } else if (fVersion == 1) {
      return fpga_read32(0x10000 + 4*ireg);
   } else if (fVersion == 2) {
      return fpga_read32(0x10000 + 4*ireg);
   } else {
      return 0;
   }
}

void Chronobox::cb_write32(int ireg, uint32_t data)
{
   if (fVersion == 0) {
      fpga_write32(0x10+0, data);
      fpga_write32(0x20+0, 0x80000000 + ireg);
      fpga_write32(0x20+0, 0);
   } else if (fVersion == 1) {
      fpga_write32(0x10000 + 4*ireg, data);
   } else if (fVersion == 2) {
      fpga_write32(0x10000 + 4*ireg, data);
   } else {
      // nothing
   }
}

void Chronobox::cb_write32bis(int ireg, uint32_t data1, uint32_t data2)
{
   if (fVersion == 0) {
      fpga_write32(0x10+0, data1);
      fpga_write32(0x20+0, 0x80000000 + ireg);
      fpga_write32(0x10+0, data2);
      fpga_write32(0x20+0, 0);
   } else if (fVersion == 1) {
      cb_write32(ireg, data1);
      cb_write32(ireg, data2);
   } else if (fVersion == 2) {
      cb_write32(ireg, data1);
      cb_write32(ireg, data2);
   } else {
      // nothing
   }
}

char* Chronobox::cb_addr(int ireg)
{
   return fpga_addr(0x10000 + 4*ireg);
}

void Chronobox::cb_reboot()
{
   uint32_t fwrev = cb_read32(0);
   uint32_t mark = 0xdeadf00d;
   cb_write32(4, mark);
   printf("cb fw rev: 0x%08x, reg4: 0x%08x\n", fwrev, cb_read32(4));
   printf("fpga reconfigure!\n");
   sleep(1);
   cb_write32(0xE, ~fwrev);
   //printf("cb regE: 0x%08x\n", cb_read32(0xE));
   sleep(1);
   printf("cb fw rev: 0x%08x, reg4: 0x%08x\n", cb_read32(0), cb_read32(4));
   sleep(1);
   printf("cb fw rev: 0x%08x, reg4: 0x%08x\n", cb_read32(0), cb_read32(4));
   sleep(1);
   printf("cb fw rev: 0x%08x, reg4: 0x%08x\n", cb_read32(0), cb_read32(4));

   if (cb_read32(4) != 0) {
      printf("error: reg4 did not reset to zero, the FPGA did not reboot!\n");
   } else {
      printf("done.\n");
   }
}

int Chronobox::cb_read_input_num()
{
   uint32_t rev = cb_read32(0);
   if (rev >= 0x5b89e4b4) {
      return cb_read32(0xF);
   } else {
      return 58;
   }
}

void Chronobox::cb_read_scaler_begin()
{
   if (fVersion == 0) {
      // write the address register
      fpga_write32(0x20+0, 0x40000000 + 8);
   } else {
      // nothing
   }
}

uint32_t Chronobox::cb_read_scaler(int iscaler)
{
   if (fVersion == 0) {
      //write32(virtual_base, 0x20+0, 0x80000000 + 8); // should not be needed
      // write data into reg8 sets scaler_addr_out
      fpga_write32(0x10+0, iscaler);
      //write32(virtual_base, 0x20+0, 0x40000000 + 8); // should not be needed
      // read reg8_scaler_data_in
      return fpga_read32(0x00+0);
   } if (fVersion == 1) {
      cb_write32(8, iscaler);
      return cb_read32(8);
   } if (fVersion == 2) {
      cb_write32(8, iscaler);
      return cb_read32(8);
   } else {
      // nothing
      return 0;
   }
}

void Chronobox::cb_arm_sync()
{
   cb_write32(32, 0x80000000);
   cb_write32(32, 0);
   printf("cb_arm_sync: sync status 0x%08x after sync arm\n", cb_read32(32));
}

void Chronobox::cb_disarm_sync()
{
   cb_write32(32, 0x40000000);
   cb_write32(32, 0);
   printf("cb_disarm_sync: sync status 0x%08x after sync disarm\n", cb_read32(32));
}

void Chronobox::cb_read_fifo(std::vector<uint32_t> *data, bool *overflow)
{
   uint32_t fifo_status = cb_read32(16);
   bool fifo_full  = fifo_status & 0x80000000;
   bool fifo_empty = fifo_status & 0x40000000;
   bool fifo_overflow_tsc  = fifo_status & 0x20000000;
   bool fifo_overflow_data = fifo_status & 0x10000000;
   int fifo_used = fifo_status & 0x00FFFFFF;
   
   //printf("fifo status: 0x%08x, full %d, empty %d, used %d\n", fifo_status, fifo_full, fifo_empty, fifo_used);

   if (fifo_empty) {
      return;
   }

   if (fifo_full)
      if (overflow)
         *overflow = true;
   
   if (fifo_overflow_tsc)
      if (overflow)
         *overflow = true;
   
   if (fifo_overflow_data)
      if (overflow)
         *overflow = true;
   
   if (fifo_full && fifo_used == 0) {
      fifo_used = 0x10;
   }

   //if (fifo_used == 256)
   //   printf("fifo status: 0x%08x, full %d, empty %d, used %d\n", fifo_status, fifo_full, fifo_empty, fifo_used);

   if (fVersion == 0) {
      for (int i=0; i<fifo_used; i++) {
         cb_write32(0, 4);
         cb_write32(0, 0);
         uint32_t v = cb_read32(17);
         //printf("read %3d: 0x%08x\n", i, v);
         data->push_back(v);
      }
   } else if (fVersion == 1) {
      // single-word read
      data->reserve(fifo_used);
      for (int i=0; i<fifo_used; i++) {
         uint32_t v = cb_read32(17);
         //printf("read %3d: 0x%08x\n", i, v);
         data->push_back(v);
      }
   } else if (fVersion == 2) {
      if (fifo_used == 0) {
         // empty fifo
      } else if (fifo_used == 1) {
         uint32_t v = cb_read32(17);
         //printf("read %3d: 0x%08x\n", i, v);
         data->push_back(v);
      } else if (0) {
         // single-word read
         data->reserve(fifo_used);
         for (int i=0; i<fifo_used; i++) {
            uint32_t v = cb_read32(17);
            //printf("read %3d: 0x%08x\n", i, v);
            data->push_back(v);
         }
      } else if (0) {
         // single word read using fifo avalon bus
         char* addr = fpga_addr(0x20000);
         data->reserve(fifo_used);
         for (int i=0; i<fifo_used; i++) {
            uint32_t v = *(uint32_t*)addr;
            //printf("read %3d: 0x%08x\n", i, v);
            data->push_back(v);
         }
      } else if (1) {
         // memcpy() read using fifo avalon bus
         char* addr = fpga_addr(0x20000);
         int to_read = fifo_used * 4;
         if (to_read >= 0x10000)
            to_read = 0x10000;
         data->resize(fifo_used);
         memcpy(data->data(), addr, to_read);
      } else if (0) {
         // busy loop for testing
         while (1) {
            cb_read32(17);
         }
      }
   }
}

void Chronobox::cb_latch_scalers()
{
   cb_write32bis(0, 1, 0);
}

void Chronobox::cb_reset_scalers()
{
   cb_write32bis(0, 2, 0);
}

void Chronobox::cb_int_clock()
{
   uint32_t reg1C = cb_read32(0x1C);
   printf("clk_ext pll status 0x%08x\n", reg1C);
   if ((reg1C & 0x40000000) != 0) {
      printf("switching from external clock to internal clock!\n");
      cb_write32(0, 1<<4);
      cb_write32(0, 0); 
      cb_write32(0, 1<<3);
      cb_write32(0, 0);
      ::sleep(1);
   }
   reg1C = cb_read32(0x1C);
   printf("clk_ext pll status 0x%08x\n", reg1C);
}

void Chronobox::cb_ext_clock()
{
   uint32_t reg1C = cb_read32(0x1C);
   printf("clk_ext pll status 0x%08x\n", reg1C);
   if ((reg1C & 0x40000000) == 0) {
      printf("switching from internal clock to external clock!\n");
      cb_write32(0, 1<<4);
      cb_write32(0, 0); 
      cb_write32(0, 1<<3);
      cb_write32(0, 0);
      ::sleep(1);
   }
   reg1C = cb_read32(0x1C);
   printf("clk_ext pll status 0x%08x\n", reg1C);
}

void Chronobox::cb_set_lemo_out_mux(int ilemo, int ifunc)
{
   assert(ilemo >= 0);
   assert(ilemo < 8);
   assert(ifunc >= 0);
   assert(ifunc < 16);

   uint32_t mux = cb_read32(29);

   uint32_t mask = 0xF << (ilemo*4);
   uint32_t bits = ifunc << (ilemo*4);

   mux &= ~mask;
   mux |= bits;

   cb_write32(29, mux);
}

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */

